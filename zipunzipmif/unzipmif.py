import os
import struct

offset=0

def output(msg):
    print('%4X: %s'%(offset,msg))
        
def Loop():
    if not(os.path.exists("out")):
        os.makedirs("out")
    for file in os.listdir():
        if(file.lower()=="default.mif"):
            output(file)
            with open(file,'rb') as f:
                offset=0
                data = iter(f.read())

                def munch(n):
                    global offset
                    offset+=n
                    return b''.join([bytes(chr(next(data)), 'latin1') for _ in range(n)])
                    
                assert struct.unpack('I', munch(4))[0]==100
                headerLength=struct.unpack('I', munch(4))[0]
                assert headerLength==0x2BF20
                number_of_files=struct.unpack('I', munch(4))[0]
                
                i=0
                files=0
                fileset_data=[None]*number_of_files
                while i<headerLength and files<number_of_files:
                    filename=munch(0x5C)
                    filename=filename[:filename.find(b'\0')].decode('latin1')
                    seekspot=struct.unpack('I', munch(4))[0]
                    filelength=struct.unpack('I', munch(4))[0]
                    fileset_data[files]=[filename,seekspot,filelength]
                    output("%s - %s: %X - %X"%(files,filename,seekspot,filelength))
                    files+=1
                    i+=0x5C+4+4
                for j in fileset_data:
                    f.seek(j[1],0)
                    with open("out\%s"%j[0],'wb') as g:
                        g.write(f.read(j[2]))
Loop()