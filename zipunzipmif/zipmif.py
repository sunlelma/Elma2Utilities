import os
import struct
import shutil
import time

headerlength=180000
        
def Loop():
    if not(os.path.exists("out")):
        print("No out/ directory found - aborting")
        return
    
    if not(os.path.exists("backup")):
        print("ok")
        os.mkdir("backup")
        print("Made backup/ directory")
        
    for file in os.listdir():
        if(file.lower()=="default.mif"):
            shutil.copy2(file,"backup/%s.mif"%time.strftime("%Y%m%d%H%M%S",time.gmtime()))
            print("Backup created in backup directory")
            break
    
    filelist=os.listdir("out")
    #filelist=sorted(filelist,key=lambda s:s.lower())
    n_files=len(filelist)
    file_pos=[None]*n_files
    file_len=[None]*n_files
    
    with open('default.mif','wb') as f:
        f.write(struct.pack('I', 100))
        f.write(struct.pack('I', headerlength))
        assert(n_files>0 and n_files<=1800)
        f.write(struct.pack('I', n_files))
        
        for i in range(headerlength):
            f.write(b'\xCD')
        f.write(struct.pack('I',0x5B7AAB9E))
        
        for i in range(n_files):
            #print(filelist[i])
            with open('out/%s'%filelist[i],'rb') as g:
                file_pos[i]=f.tell()
                f.write(g.read())
                file_len[i]=g.tell()
        f.write(struct.pack('I',0x5B7AAB9E))
        
        for i in range(n_files):
            f.seek(12+100*i)
            f.write(bytes(filelist[i], 'latin1'))
            f.write(b'\0')
            f.seek(12+100*i+92)
            f.write(struct.pack('I', file_pos[i]))
            f.write(struct.pack('I', file_len[i]))
    return
    
Loop()