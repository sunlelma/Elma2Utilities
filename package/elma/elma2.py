from abc import ABCMeta
import random
import struct
from elma.models import Picture
from elma.models import Point
from elma.utils import null_padded
from elma.models import Obj
from elma.models import Polygon
from elma.models import Level
from copy import deepcopy
from elma.models import Frame
from elma.models import GroundTouchAEvent
from elma.models import GroundTouchBEvent
from elma.models import LeftVoltEvent
from elma.models import ObjectTouchEvent
from elma.models import Replay
from elma.models import RightVoltEvent
from elma.models import TurnEvent
try:
    bytes('A', 'latin1')
except TypeError:
    bytes = lambda a, b: a  # noqa
    chr = lambda a: a if type(a) == str else chr(a)  # noqa

class Obj2(object):
    """
    Represent an Elastomania 2 object

    Attributes:
        point (Point): The 2D Point that represents the position of the object.
        type (int): The type of the object, which should be one of:
            Obj2.FLOWER - Obj2.Teleport
        group_id (int): The Group ID that is activated by a Group Switch or the
            ID that will activate a Switch
        path_id (int): The Path ID activated by a Switch, or the Condition 2:
            Path ID for a Group Switch object
        opener_closer (int): Determines whether a Switch opens or closes a path
            (Obj2.SWITCH_OPENER, Obj2.SWITCH_CLOSER), or whether a Turn is left
            or right (Obj2.TURN_LEFT, Obj2.TURN_RIGHT), or whether a teleport
            is a source or destination (Obj2.TELEPORT_A, Obj2.TELEPORT_B)
        card (int): Determines the colour of a card for Card, Switch and Group
            Switch objects
        condition_state (int): Determines Condition 2: State for Group Switch
            objects.
        platform_id (int): Determines the Platform ID for Platform Reference
            objects.
        comments (string): A comment section to add extra information for the
            editor when designing the level. Supports up to 300 bytes of text.
    """

    FLOWER = 1
    FOOD = 2
    KILLER = 3
    START = 4
    BONUS = 5
    CARD = 6
    SWITCH = 7
    GROUP_SWITCH = 8
    PLATFORM_REFERENCE = 9
    STAGE = 10
    TURN = 11
    JUMP = 12
    TELEPORT = 13

    NO_CARD = 0
    RED = 1
    YELLOW = 2
    BLUE = 3
    
    UP = 1
    DOWN = 0
    
    SWITCH_OPENER = 1
    SWITCH_CLOSER = 0
    TURN_LEFT = 1
    TURN_RIGHT = 0
    TELEPORT_A = 1
    TELEPORT_B = 0

    def __init__(self, point, type,
                 group_id=0, path_id=0, opener_closer=SWITCH_OPENER, card=NO_CARD, 
                 condition_state=UP, platform_id=0, comments=''):
        self.point = point
        self.type = type
        self.group_id = group_id
        self.path_id = path_id
        self.opener_closer = opener_closer
        self.card = card
        self.condition_state = condition_state
        self.platform_id = platform_id
        self.comments = comments

    def __repr__(self):
        if(self.type==Obj2.FLOWER):
            return (
                'Obj2(point: %s, type: Flower)' %
                (self.point))
        if(self.type==Obj2.FOOD):
            return (
                'Obj2(point: %s, type: Food)' %
                (self.point))
        if(self.type==Obj2.KILLER):
            return (
                'Obj2(point: %s, type: Killer)' %
                (self.point))
        if(self.type==Obj2.START):
            return (
                'Obj2(point: %s, type: Start)' %
                (self.point))
        if(self.type==Obj2.BONUS):
            return (
                'Obj2(point: %s, type: Bonus)' %
                (self.point))
        if(self.type==Obj2.CARD):
            return (
                'Obj2(point: %s, type: Card, card: %s)' %
                (self.point, self.card))
        if(self.type==Obj2.SWITCH):
            return (
                'Obj2(point: %s, type: Switch, group_id: %s, opener_closer: %s, path_id: %s, card: %s)' %
                (self.point, self.group_id, self.path_id, self.opener_closer, self.card))
        if(self.type==Obj2.GROUP_SWITCH):
            return (
                'Obj2(point: %s, type: Group Switch, group_id: %s, card: %s, condition_path_id: %s, condition_state: %s)' %
                (self.point, self.group_id, self.card, self.condition_path_id, self.condition_state))
        if(self.type==Obj2.PLATFORM_REFERENCE):
            return (
                'Obj2(point: %s, type: Platform Reference, platform_id: %s)' %
                (self.point, self.platform_id))
        if(self.type==Obj2.STAGE):
            return (
                'Obj2(point: %s, type: Stage)' %
                (self.point))
        if(self.type==Obj2.TURN):
            return (
                'Obj2(point: %s, type: Turn, opener_closer: %s)' %
                (self.point, self.opener_closer))
        if(self.type==Obj2.JUMP):
            return (
                'Obj2(point: %s, type: Jump)' %
                (self.point))
        if(self.type==Obj2.TELEPORT):
            return (
                'Obj2(point: %s, type: Teleport, group_id: %s, opener_closer: %s)' %
                (self.point, self.group_id, self.opener_closer))
        return (
            'Obj2(point: %s, type: %s, group_id: %s, path_id: %s, opener_closer: %s, card: %s, condition_path_id: %s, condition_state: %s, platform_id: %s)' %
            (self.point, self.type, self.path_id, self.opener_closer, self.card, self.condition_path_id, self.condition_state, self.platform_id))
            
    def __eq__(self, other_obj):
        return (self.point == other_obj.point and
                self.type == other_obj.type and
                self.group_id == other_obj.group_id and
                self.path_id == other_obj.path_id and
                self.opener_closer == other_obj.opener_closer and
                self.card == other_obj.card and
                self.condition_path_id == other_obj.condition_path_id and
                self.platform_id == other_obj.platform_id and
                self.comments == other_obj.comments)


class Polygon2(object):
    """
    Represents an Elastomania 2 polygon.

    Attributes:
        points (list): A list of Points defining the polygon contour.
        type (int): Polygon type (NORMAL - BORDER)
        platform_id (int): Platform ID
        style_index (int): Normal/Platform/Border's Style IndexError
        override_force_solid (int): Override - Polygon interacts with biker
            OVERRIDE_FORCE_SOLID_ON or NO_OVERRIDE
        override_border (int): Override - Border on, Border off, and Whole
            polygon border colored. NO_OVERRIDE of OVERRIDE_BORDER_ON/OFF/COLOR
        override_distance (int): New distance, or 0 for default style distance
        path_sub_type (int): Path Sub Type (DOOR - CONTINUOUS_TWOWAY)
        path_id (int): Path Platform ID
        path_speed_at_top (int): Path Speed At Top or Path Speed
        path_speed_at_bottom (int): Path Speed At Bottom
        path_open_delay (int): Path Open Delay
        path_close_delay (int): Path Close Delay
        path_reopen_delay (int): Path ReOpen Delay
        path_reclose_delay (int): Path ReClose Delay
        path_initially (int): Path Initially - initial position described by
            INITIALLY_UP or INITIALLY_DOWN
        path_no_of_platforms (int): Path Number of Platforms
        comments (string): A comment section to add extra information for the
            editor when designing the level. Supports up to 300 bytes of text.
    """
    
    NORMAL = 1
    INACTIVE = 2
    PLATFORM = 3
    PATH = 4
    BORDER = 5
    
    NO_OVERRIDE = 0
    OVERRIDE_FORCE_SOLID_ON = 1
    
    NO_OVERRIDE = 0
    OVERRIDE_BORDER_ON = 1
    OVERRIDE_BORDER_OFF = 2
    OVERRIDE_BORDER_COLOR = 3
    
    DOOR = 1
    ELEVATOR = 2
    FALLING = 3
    CONTINUOUS_ONEWAY = 4
    CONTINUOUS_TWOWAY = 5
    
    INITIALLY_UP = 1
    INITIALLY_DOWN = 0
    
    def __init__(self):
        self.points = []
        self.type = 1
        self.platform_id = 0
        self.style_index = 0
        self.override_force_solid = 0
        self.override_border = 0
        self.override_distance = 0
        self.path_sub_type = 0
        self.path_id = 0
        self.path_speed_at_top = 0
        self.path_speed_at_bottom = 0
        self.path_open_delay = 0
        self.path_close_delay = 0
        self.path_reopen_delay = 0
        self.path_reclose_delay = 0
        self.path_initially = 0
        self.path_no_of_platforms = 0
        self.comments = ''

    def __repr__(self):
        if(self.type == Polygon2.PATH):
            return (
                'Polygon2(points: %s, type: %s, platform_id: %s, path_sub_type: %s, path_id: %s, path_speed_at_top: %s, path_speed_at_bottom: %s, path_open_delay:%s, path_close_delay: %s, path_reopen_delay: %s, path_reclose_delay: %s, path_initially: %s, path_no_of_platforms: %s)' % (self.points, self.type, self.platform_id, self.path_sub_type, self.path_id, self.path_speed_at_top, self.path_speed_at_bottom, self.path_open_delay, self.path_close_delay, self.path_reopen_delay, self.path_reclose_delay, self.path_initially, self.path_no_of_platforms))
        else:
            return (
                'Polygon2(points: %s, type: %s, platform_id: %s, style_index: %s, override_force_solid: %s, override_border: %s, override_distance: %s)' %
                (self.points, self.type, self.platform_id, self.style_index, self.override_force_solid, self.override_border, self.override_distance))
                
class Line(object):
    """
    Represent an Elastomania 2 line

    Attributes:
        point1 (Point): The 2D Point that represents the initial coordinate
        point2 (Point): The 2D Point that represents the destination coordinate
        texture_id (int): The texture id to draw to the right of the line. If
            the right of the line is "sky"/nothing to draw, the texture_id
            should be NO_TEXTURE. This will also draw nothing in the minimap.
            NO_TEXTURE - YELLOW
        unsolid (boolean): Whether the kuski goes through the line or touches
        distance (int): Distance
        invisible (boolean): Note that the line will still be drawn in the
            minimap however. In general, in normal circumstances, invisible
            lines should not be included as a Line object at all. This property
            is unused by the default editor as the lines are simply not
            included, or the texture_id is set as NO_TEXTURE instead
    """

    #texture_id cheat sheet: http://kopasite.net/up/2/StyleCheatSheet.pdf
    NO_TEXTURE = 0
    GROUND_BORDER = 10
    YELLOW_STRIPES = 11
    GROUND = 100
    GREEN = 101
    PLATFORM = 102
    BLUE_SKY = 103
    PLATFORM_BORDER = 104
    GROUND_VIEW = 105
    SKY_VIEW = 106
    HELMET = 120
    YELLOW_EDGE = 122
    YELLOW = 124
    
    def __init__(self, point1, point2, texture_id, unsolid, distance, invisible=False):
        self.point1 = point1
        self.point2 = point2
        self.texture_id = texture_id
        self.unsolid = unsolid
        self.distance = distance
        self.invisible = invisible

    def __repr__(self):
        return (
            'Line(point1: %s, point2: %s, texture_id: %s, unsolid: %s, distance: %s, invisible: %s)' %
            (self.point1, self.point2, self.texture_id, self.unsolid, self.distance, self.invisible))
            
    def __eq__(self, other_obj):
        return (self.point1 == other_obj.point1 and
                self.point2 == other_obj.point2 and
                self.texture_id == other_obj.texture_id and
                self.unsolid == other_obj.unsolid and
                self.distance == other_obj.distance and
                self.invisible == other_obj.invisible)

class Platform(object):
    """
    Represent an Elastomania 2 platform

    Attributes:
        lines (list): A list of lines drawing all polygons of the corresponding
            platform_id (even if several polygons), but everything is 0 except
            point1 and point2
        platform_reference_point: A Point object of the object's xy
        int112-156 (int): See http://wiki.elmaonline.net/Elma_2/Technical#Platform_Object
        platform_id (int): Platform Id
        texture_id (int): Main Texture ID of the platform
        border_texture_id (int):  exture ID of the platform border
    """
    
    def __init__(self):
        self.lines = []
        self.platform_reference_point = None
        self.int112 = 0
        self.int116 = 0
        self.int120 = 0
        self.int124 = 0
        self.platform_id = 0
        self.int132 = 0
        self.int136 = 0
        self.int140 = 0
        self.int144 = 0
        self.int148 = 0
        self.int152 = 0
        self.int156 = 0
        self.texture_id = 0
        self.border_texture_id = 0

    def __repr__(self):
        return (
            'Platform(platform_id: %s)'%(platform_id))
    
    def X_Min(self):
        return min(self.lines, key=lambda i: i.point1.x).point1.x
        
    def Y_Min(self):
        return min(self.lines, key=lambda i: i.point1.y).point1.y
        
    def X_Max(self):
        return max(self.lines, key=lambda i: i.point1.x).point1.x
        
    def Y_Max(self):
        return max(self.lines, key=lambda i: i.point1.y).point1.y
        
        
class Path(object):
    """
    Represent an Elastomania 2 platform

    Attributes:
        lines (list): A list of lines drawing all polygons of the corresponding
            platform_id (even if several polygons), with distance=0 and
            invisible=False
        platform_reference_point: A Point object of the object's xy
        int112-156 (int): See http://wiki.elmaonline.net/Elma_2/Technical#Platform_Object
        platform_id (int): Platform Id
        texture_id (int): Main Texture ID of the platform
    """

    DOOR = 1
    ELEVATOR = 2
    FALLING = 3
    CONTINUOUS_ONEWAY = 4
    CONTINUOUS_TWOWAY = 5
    
    INITIALLY_UP = 1
    INITIALLY_DOWN = 0
    
    def __init__(self):
        self.path_id = 0
        self.path_platform_id = 0
        self.path_sub_type = 0
        self.path_speed_at_top = 0
        self.path_speed_at_bottom = 0
        self.path_open_delay = 0
        self.path_close_delay = 0
        self.path_reopen_delay = 0
        self.path_reclose_delay = 0
        self.path_initially = 0
        self.path_no_of_platforms = 0
        self.forward_frames = 0
        self.total_frames = 0
        self.int96 = 0
        self.int100 = 0
        self.int104 = 0
        self.int108 = 0
        self.int112 = 0
        self.int136 = 0
        self.movement_x = []
        self.movement_y = []
        self.velocity_x = []
        self.velocity_y = []
        self.platform_x = []
        self.platform_y = []

    def __repr__(self):
        return (
            'Path(path_id: %s, path_sub_type: %s, path_platform_id: %s, path_speed_at_top: %s, path_speed_at_bottom: %s, path_open_delay:%s, path_close_delay: %s, path_reopen_delay: %s, path_reclose_delay: %s, path_initially: %s, path_no_of_platforms: %s)' %
            (self.path_id, self.path_sub_type, self.path_platform_id, self.path_speed_at_top, self.path_speed_at_bottom, self.path_open_delay, self.path_close_delay, self.path_reopen_delay, self.path_reclose_delay, self.path_initially, self.path_no_of_platforms))

class Level2(object):
    """
    Represent an Elastomania 2 level.

    Attributes:
        polygons (list): A list of Polygon2 representing polygons in the level.
        objects (list): A list of Obj2 representing objects in the level.
        pictures (list): A list of Picture representing pictures in the level.
        name (string): The name of level, which should be no longer than 50
            characters long. Not displayed in-game
        lgr (string): The name of the LGR used for this level, which should be
            no longer than 10 characters long. Always default in elma2 so far
        face_right (bool): Whether the bike should turn automatically at start
        
        topology_error (bool): If there is a topology error, all subsequent
            variables are excluded from the saved level file
        lines (list): A list of Line - solid and non-solid lines from Normal and
            Border polygons
        platforms (list): A list of Platform in the level
        paths (list): A list of Path in the level
        level_id (int): A unique POSITIVE SIGNED 32bit integer level identifier.
        unknown1 (float): Probably always 60.0
        unknown2 (float): Unknown negative number always around this number
        unknown3 (float): Unknown negative number always around this number
    """
    def __init__(self):
        self.polygons = []
        self.objects = []
        self.pictures = []
        self.name = 'Unnamed'
        self.lgr = 'default'
        self.face_right = False
        self.locked = False
        
        self.topology_error=False
        self.lines = []
        self.platforms = []
        self.paths = []
        self.level_id = random.randint(0, (2 ** 31) - 1)
        self.unknown1 = 60.0
        self.unknown2 = -406.64166666666665
        self.unknown3 = -74.99166666666666 
        
    def __repr__(self):
        return (('Level(level_id: %s)') %
                (self.level_id))

                
def pack_level2(item):
    """
    Pack a level-related item to its binary representation readable by
    Elastomania 2.
    """
    
    if isinstance(item, Point):
        return b''.join([struct.pack('d', item.x),
                        struct.pack('d', item.y)])
    if isinstance(item, Polygon2):
        return b''.join([struct.pack('I', item.type),
                        struct.pack('I', item.path_sub_type),
                        struct.pack('I', item.platform_id),
                        struct.pack('I', item.path_id),
                        struct.pack('I', item.path_speed_at_top),
                        struct.pack('I', item.path_speed_at_bottom),
                        struct.pack('I', item.path_open_delay),
                        struct.pack('I', item.path_close_delay),
                        struct.pack('I', item.path_reopen_delay),
                        struct.pack('I', item.path_reclose_delay),
                        struct.pack('I', item.path_initially),
                        struct.pack('I', item.path_no_of_platforms),
                        struct.pack('I', item.override_force_solid),
                        struct.pack('I', item.style_index),
                        struct.pack('I', 1 if item.override_border == Polygon2.OVERRIDE_BORDER_ON else 0),
                        struct.pack('I', 1 if item.override_border == Polygon2.OVERRIDE_BORDER_OFF else 0),
                        struct.pack('I', 1 if item.override_border == Polygon2.OVERRIDE_BORDER_COLOR else 0),
                        struct.pack('I', item.override_distance),
                        struct.pack('I', 0),
                        null_padded(item.comments,300),
                        struct.pack('I', len(item.points))] + [
                        pack_level2(point) for point in item.points])
    if isinstance(item, Obj2):
        return b''.join([pack_level2(item.point),
                        struct.pack('I', item.type),
                        struct.pack('I', item.group_id),
                        struct.pack('I', item.path_id),
                        struct.pack('I', item.opener_closer),
                        struct.pack('I', item.card),
                        struct.pack('I', item.condition_state),
                        struct.pack('I', item.platform_id),
                        null_padded(item.comments,300)])
    if isinstance(item, Line):
        dx=item.point2.x-item.point1.x
        dy=item.point2.y-item.point1.y
        length=(dx**2+dy**2)**0.5
        dx_unit=dx/length
        dy_unit=dy/length
        return b''.join([pack_level2(item.point1),
                        struct.pack('d', dx),
                        struct.pack('d', dy),
                        struct.pack('d', dx_unit),
                        struct.pack('d', dy_unit),
                        struct.pack('d', length),
                        struct.pack('I', item.texture_id),
                        struct.pack('I', 1 if item.unsolid else 0),
                        struct.pack('I', item.distance),
                        struct.pack('I', 1 if item.invisible else 0)])
    if isinstance(item, Platform):
        xmin=item.X_Min()-item.platform_reference_point.x
        ymin=item.Y_Min()-item.platform_reference_point.y
        xmax=item.X_Max()-item.platform_reference_point.x
        ymax=item.Y_Max()-item.platform_reference_point.y
        return b''.join([struct.pack('B', 1),
                        struct.pack('d', xmin-0.05),
                        struct.pack('d', ymin-0.05),
                        struct.pack('d', xmin),
                        struct.pack('d', ymin),
                        struct.pack('d', xmax),
                        struct.pack('d', ymax),
                        struct.pack('d', xmin-0.33),
                        struct.pack('d', ymin-0.33),
                        struct.pack('d', xmin),
                        struct.pack('d', ymin),
                        struct.pack('d', xmax),
                        struct.pack('d', ymax),
                        pack_level2(item.platform_reference_point),
                        struct.pack('I', item.int112),
                        struct.pack('I', item.int116),
                        struct.pack('I', item.int120),
                        struct.pack('I', item.int124),
                        struct.pack('I', item.platform_id),
                        struct.pack('I', item.int132),
                        struct.pack('I', item.int136),
                        struct.pack('I', item.int140),
                        struct.pack('I', item.int144),
                        struct.pack('I', item.int148),
                        struct.pack('I', item.int152),
                        struct.pack('I', item.int156),
                        struct.pack('I', item.texture_id),
                        struct.pack('I', item.border_texture_id),
                        struct.pack('I', 1478256389),
                        struct.pack('I', 0),
                        struct.pack('I', len(item.lines)),
                    ] + [pack_level2(line) for line in item.lines])
    if isinstance(item, Path):
        assert(len(item.movement_x)==item.total_frames)
        assert(len(item.movement_y)==item.total_frames)
        assert(len(item.velocity_x)==item.total_frames)
        assert(len(item.velocity_y)==item.total_frames)
        assert(len(item.platform_x)==item.total_frames)
        assert(len(item.platform_y)==item.total_frames)
        return b''.join([struct.pack('B', 1),
                        null_padded('',24),
                        struct.pack('I', item.path_id),
                        struct.pack('I', item.path_platform_id),
                        struct.pack('I', item.path_sub_type),
                        struct.pack('I', item.path_speed_at_top),
                        struct.pack('I', item.path_speed_at_bottom),
                        struct.pack('I', item.path_open_delay),
                        struct.pack('I', item.path_close_delay),
                        struct.pack('I', item.path_reopen_delay),
                        struct.pack('I', item.path_reclose_delay),
                        struct.pack('I', item.path_initially),
                        struct.pack('I', item.path_no_of_platforms),
                        null_padded('',16),
                        struct.pack('I', item.forward_frames),
                        struct.pack('I', item.total_frames),
                        null_padded('',4),
                        struct.pack('I', item.int96),
                        struct.pack('I', item.int100),
                        struct.pack('I', item.int104),
                        struct.pack('I', item.int108),
                        struct.pack('I', item.int112),
                        null_padded('',20),
                        struct.pack('I', item.int136),
                        struct.pack('I', item.int136),
                        struct.pack('I', item.int136),
                        struct.pack('I', item.int136),
                        struct.pack('I', item.int136),
                        struct.pack('I', item.int136),
                        struct.pack('I', 1236278137),
                        null_padded('',4),
                    ] + [struct.pack('i', data) for data in item.movement_x] +
                        [b'\x15'] +
                        [struct.pack('i', data) for data in item.movement_y] +
                        [b'\x15'] +
                        [struct.pack('i', data) for data in item.velocity_x] +
                        [b'\x15'] +
                        [struct.pack('i', data) for data in item.velocity_y] +
                        [b'\x15'] +
                        [struct.pack('i', data) for data in item.platform_x] +
                        [b'\x15'] +
                        [struct.pack('i', data) for data in item.platform_y] +
                        [b'\x15'])
    if isinstance(item, Picture):
        return b''.join([
            null_padded(item.picture_name, 10),
            null_padded(item.texture_name, 10),
            null_padded(item.mask_name, 10),
            pack_level2(item.point),
            struct.pack('I', item.distance),
            struct.pack('I', item.clipping)])
    level=item
    compile_object=b''
    if(level.topology_error == False):
        compile_object = b''.join([
                struct.pack('I', len(level.lines)),
            ] + [pack_level2(line) for line in level.lines] +
                [struct.pack('d', level.unknown1),
                struct.pack('i', level.level_id),
            ] + [pack_level2(platform) for platform in level.platforms] +
                [struct.pack('B', 0),
                b'\x58',
                struct.pack('d', level.unknown2),
                struct.pack('d', level.unknown3),
            ] + [pack_level2(path) for path in level.paths] +
                [struct.pack('B', 0)])
    return b''.join([
        bytes('POT35','latin1'),
        struct.pack('I', 0 if level.locked else 30),
        struct.pack('H', 1 if level.topology_error else 0),
        struct.pack('I', 1 if level.face_right else 0),
        null_padded(level.name, 51),
        null_padded(level.lgr, 16),
        null_padded('textura',10),
        null_padded('qgrass',10),
        null_padded('',20),
        struct.pack('I', 135),
        struct.pack('d', len(level.polygons) + 0.4643643),
    ] + [pack_level2(polygon) for polygon in level.polygons] +
        [compile_object,
        b'\x2B',
        struct.pack('I', 192356446),
        struct.pack('d', len(level.objects) + 0.4643643),
    ] + [pack_level2(obj) for obj in level.objects] + [
        struct.pack('d', len(level.pictures) + 0.2345672),
    ] + [pack_level2(picture) for picture in level.pictures])
    

class Frame2(object):
    """
    Represent an Elastomania 2 replay frame

    Attributes:
        position (Point): x (float) and y (float) coordinates of the center of the bike
        left_wheel_position (Point): x (short) and y (short) coordinates of the left wheel
            in relation to the center of the bike; not affected by rotation
        right_wheel_position (Point): x (short) and y (short)
        head_position (Point): x (short) and y (short) coordinates of the head
            in relation to the center of the bike; not affected by rotation
        left_wheel_rotation (short): 0-9999 representing the rotation of the wheel,
            where 0 is in neutral position
        right_wheel_rotation (short): same
        bike_rotation (short): 0-9999 representing the rotation of the bike and
            the rotation of the head
        back_wheel_rotation_speed (short): 1000-2000 representing the absolute
            value of the spinspeed of the back wheel (depending on facing
            direction). 1000 corresponds to no spin and 2000 corresponds to
            maximum throttle. The speed is capped at 2000.
        back_wheel_air_time (int): The number of frames since this back wheel
            has touched any polygon, or the number of frames since the first
            frame. 0 if the back wheel is touching the ground.
    """
    
    def __init__(self):
        self.position = Point(0, 0)
        self.left_wheel_position = Point(0, 0)
        self.right_wheel_position = Point(0, 0)
        self.head_position = Point(0, 0)
        self.left_wheel_rotation = 0
        self.right_wheel_rotation = 0
        self.bike_rotation = 0
        self.back_wheel_rotation_speed = 0
        self.back_wheel_air_time = 0

    def __repr__(self):
        return (
            'Frame(position: %s, left_wheel_position: %s, right_wheel_position: %s, head_position: %s, left_wheel_rotation: %s, right_wheel_rotation: %s, bike_rotation: %s, back_wheel_rotation_speed: %s, back_wheel_air_time: %s)' %
            (self.position, self.left_wheel_position, self.right_wheel_position, self.head_position, self.left_wheel_rotation, self.right_wheel_rotation, self.bike_rotation, self.back_wheel_rotation_speed, self.back_wheel_air_time))
            
    def __eq__(self, other_obj):
        return (self.position == other_obj.position and
                self.left_wheel_position == other_obj.left_wheel_position and
                self.right_wheel_position == other_obj.right_wheel_position and
                self.head_position == other_obj.head_position and
                self.left_wheel_rotation == other_obj.left_wheel_rotation and
                self.right_wheel_rotation == other_obj.right_wheel_rotation and
                self.bike_rotation == other_obj.bike_rotation and
                self.back_wheel_rotation_speed == other_obj.back_wheel_rotation_speed and
                self.back_wheel_air_time == other_obj.back_wheel_air_time)
                
class Event2(object):
    """
    Represent an Elastomania 2 replay event

    Attributes:
        frame_of_event (int): The frame at which this event is activated
        event_id (int): A number identifying the type of event that has
            occurred (from OBJECT_TOUCH to THRUST_END)
        object_id (int): If event_id is OBJECT_TOUCH, the array number of the
            object that was touched. Or else -1
        
    """
    
    OBJECT_TOUCH = 0
    DEATH = 2
    RIGHT_VOLT = 3
    LEFT_VOLT = 4
    TURN = 5
    THRUST_START = 6
    THRUST_END = 7
    
    def __init__(self):
        self.frame_of_event = 0
        self.event_id = 0
        self.object_id = 0

    def __repr__(self):
        return (
            'Frame(frame_of_event: %s, event_id: %s, object_id: %s)' %
            (self.frame_of_event, self.event_id, self.object_id))
            
    def __eq__(self, other_obj):
        return (self.frame_of_event == other_obj.frame_of_event and
                self.event_id == other_obj.event_id and
                self.object_id == other_obj.object_id)

class Replay2(object):
    """
    Represent an Elastomania 2 replay.

    Attributes:
        frames (list): A sequential list of Frame2 representing each replay frame.
        events (list): A sequential list of Event2 representing each event.
        level_id (int): The randomized level_id of the .lev file to make sure
            level version has not changed since the replay was saved
        is_internal (Boolean): Wh
        level_name (string): If the level is not an internal, the filename of
            the level (e.g. test.lev), else unused variable
        pack_id (int): If the level is an internal level, the pack A-D
        pack_level_number (int): If the level is an internal level, the level
            number of the pack from 0 (i.e. 0-7)
    """
    
    A=0
    B=1
    C=2
    D=3
    
    def __init__(self):
        self.frames = []
        self.events = []
        self.level_id = []
        self.restore_points_used = False
        self.is_internal = False
        self.level_name = ''
        self.pack_id = 0
        self.pack_level_number = 0
        
    def __repr__(self):
        return (('Replay(level_id: %s)') %
                (self.level_id))

def pack_replay2(item):
    """
    Pack a replay-related item to its binary representation readable by
    Elastomania 2.
    """
    
    if isinstance(item, Frame2):
        return b''.join([struct.pack('f', item.position.x),
                        struct.pack('f', item.position.y),
                        struct.pack('h', item.left_wheel_position.x),
                        struct.pack('h', item.left_wheel_position.y),
                        struct.pack('h', item.right_wheel_position.x),
                        struct.pack('h', item.right_wheel_position.y),
                        struct.pack('h', item.head_position.x),
                        struct.pack('h', item.head_position.y),
                        struct.pack('h', item.rotation),
                        struct.pack('h', item.short1),
                        struct.pack('h', item.short2),
                        struct.pack('h', item.short3),
                        struct.pack('i', item.int1)])    
    if isinstance(item, Event2):
        return b''.join([struct.pack('I', item.frame_of_event),
                        struct.pack('I', item.event_id),
                        struct.pack('i', item.object_id if item.event_id == 0 else -1),
                        struct.pack('I', 0)])
            
    replay=item
    return b''.join([
        struct.pack('I',0x211A3591),
        struct.pack('I',0x66),
        struct.pack('I',replay.restore_points_used),
        struct.pack('I',0x2C),
        struct.pack('I',replay.level_id),
        struct.pack('I',replay.is_internal),
        struct.pack('I',replay.pack_id),
        struct.pack('I',replay.pack_level_number),
        null_padded(replay.level_name, 28),
        struct.pack('I',len(replay.frames)),
        struct.pack('I',len(replay.events)),
    ] + [pack_replay2(frame) for frame in replay.frames
    ] + [pack_replay2(event) for event in replay.events] + [
        struct.pack('I',0x211A3591)
    ])

def downgrade_level_version(level2,keep_borders=True,grass_borders=True,draw_paths=True,platform_at_each_vertex=True,keep_misc_objects=True,misc_objects_as_killers=False,draw_inactive=True,remove_widest_poly=True):
    """Converts elma2.lev to elma1.lev
    returns deletedobjects which is a list of the indices of the deleted objects
    so that replay files can be correctly converted"""
    
    def get_platform_poly(platform,object,path_point):
        shiftx=path_point.x-object.point.x
        shifty=path_point.y-object.point.y
        points2=deepcopy(platform.points)
        for point in points2:
            point.x += path_point.x-object.point.x
            point.y += path_point.y-object.point.y
        return Polygon(points2,False)
    
    deletedobjects=[]
    
    level1=Level()
    level1.level_id=level2.level_id
    level1.name= level2.name
    level1.lgr = level2.lgr
    level1.pictures=level2.pictures[:]
    i=0
    for object in level2.objects:
        if(object.type == Obj2.PLATFORM_REFERENCE):
            deletedobjects.append(i)
        elif(object.type <= Obj2.START):
            level1.objects.append(Obj(object.point,object.type))
        elif(object.type <= Obj2.CARD):
            level1.objects.append(Obj(object.point,Obj.FOOD))
        elif(keep_misc_objects):
            if(misc_objects_as_killers):
                level1.objects.append(Obj(object.point,Obj.KILLER))
            else:
                level1.objects.append(Obj(object.point,Obj.FOOD))
        else:
            deletedobjects.append(i)
        i+=1
            
    for polygon in level2.polygons:
        if(polygon.type == Polygon2.PATH):
            if(draw_paths):
                level1.polygons.append(Polygon(polygon.points[:],True))
            continue
        if(polygon.type == Polygon2.INACTIVE):
            if(draw_inactive):
                level1.polygons.append(Polygon(polygon.points[:],True))
        if(polygon.type == Polygon2.PLATFORM):
            if(level2.topology_error):
                level1.polygons.append(Polygon(polygon.points[:],False))
            continue
        if(polygon.type == Polygon2.BORDER):
            if(keep_borders):
                level1.polygons.append(Polygon(polygon.points[:],grass_borders))
            continue
        level1.polygons.append(Polygon(polygon.points[:],False))
    if(not(level2.topology_error)):
        for path in level2.paths:
            path_poly=None
            plat_polys=[]
            for polygon in level2.polygons:
                if(polygon.type == Polygon2.PATH and path.path_id == polygon.path_id):
                    path_poly=polygon
                    continue
                if(polygon.type == Polygon2.PLATFORM and path.path_platform_id == polygon.platform_id):
                    plat_polys.append(polygon)
            platform_object=None
            for object in level2.objects:
                if(object.type == Obj2.PLATFORM_REFERENCE and path.path_platform_id == object.platform_id):
                    platform_object=object
                    break
            
            if(platform_at_each_vertex==False):
                if(path.path_initially == Path.INITIALLY_UP):
                    for platform in plat_polys:
                        level1.polygons.append(get_platform_poly(platform,platform_object,path_poly.points[0]))
                else:
                    i=len(path_poly.points)-1
                    for platform in plat_polys:
                        level1.polygons.append(get_platform_poly(platform,platform_object,path_poly.points[i]))
            else:
                first=True
                for point in path_poly.points:
                    if(first):
                        first=False
                        continue
                    for platform in plat_polys:
                        level1.polygons.append(get_platform_poly(platform,platform_object,point))
    if(remove_widest_poly):
        widestpoly=None
        best_width=-1
        for polygon in level1.polygons:
            minx=min(polygon.points, key=lambda p:p.x)
            maxx=max(polygon.points, key=lambda p:p.x)
            width=maxx.x-minx.x
            if(width>best_width):
                best_width=width
                widestpoly=polygon
        level1.polygons.remove(widestpoly)
    return level1,deletedobjects

def downgrade_replay_version(replay2,level1,level2,deletedobjects):

    def find_new_object_touch_id(object_index1,level1,obj_n):
        target_index=0
        for i in range(Obj.KILLER - level1.objects[object_index1].type):
            target_index+=obj_n[i]
        for i in range(object_index1):
            if(level1.objects[i].type == level1.objects[object_index1].type):
                target_index += 1
        return target_index
        
    replay1=Replay()
    replay1.level_id = level2.level_id
    replay1.level_name = replay2.level_name
    
    eventi=0
    event_n=len(replay2.events)
    gassing=False
    facingright=level2.face_right
    i=0
    frame2time=273/625/120 #120 fps with (273/625) conversion ratio used in elma1
    
    for frame2 in replay2.frames:
        while(eventi<event_n and i==replay2.events[eventi].frame_of_event):
            if(replay2.events[eventi].event_id == Event2.TURN):
                facingright=not facingright
            if(replay2.events[eventi].event_id == Event2.THRUST_START):
                gassing=True
            if(replay2.events[eventi].event_id == Event2.THRUST_END):
                gassing=False
            eventi+=1
        if(i%4 == 0):
            frame1=Frame()
            frame1.position=frame2.position
            frame1.left_wheel_position=frame2.left_wheel_position
            frame1.right_wheel_position=frame2.right_wheel_position
            frame1.head_position=frame2.head_position
            frame1.rotation=int(frame2.bike_rotation)
            frame1.left_wheel_rotation=frame2.left_wheel_rotation*256//10000
            frame1.right_wheel_rotation=frame2.right_wheel_rotation*256//10000
            frame1.is_gasing=gassing
            frame1.is_turned_right=facingright
            frame1.player1_has_flag=1
            frame1.flag_holder_losing_flag=0
            frame1.spring_sound_effect_volume=0
            replay1.frames.append(frame1)
        i+=1
    
    obj_n = [sum(1 for obj in level1.objects if obj.type == Obj.KILLER),
            sum(1 for obj in level1.objects if obj.type == Obj.FOOD),
            sum(1 for obj in level1.objects if obj.type == Obj.FLOWER)]
        
    for event2 in replay2.events:
        if(event2.event_id==Event2.OBJECT_TOUCH):
            newevent=ObjectTouchEvent()
            newevent.object_number=event2.object_id
            abort=False
            for item in deletedobjects:
                if(item<event2.object_id):
                    newevent.object_number-=1
                elif(item==event2.object_id):
                    abort=True
                    break
            if(abort):
                continue
            newevent.object_number = find_new_object_touch_id(newevent.object_number,level1,obj_n)
            newevent.time=event2.frame_of_event*frame2time
            replay1.events.append(newevent)
        elif(event2.event_id==Event2.RIGHT_VOLT):
            newevent=RightVoltEvent()
            newevent.time=event2.frame_of_event*frame2time
            replay1.events.append(newevent)
        elif(event2.event_id==Event2.LEFT_VOLT):
            newevent=LeftVoltEvent()
            newevent.time=event2.frame_of_event*frame2time
            replay1.events.append(newevent)
        elif(event2.event_id==Event2.TURN):
            newevent=TurnEvent()
            newevent.time=event2.frame_of_event*frame2time
            replay1.events.append(newevent)
    return replay1