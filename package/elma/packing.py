from elma.constants import END_OF_DATA_MARKER
from elma.constants import END_OF_FILE_MARKER
from elma.constants import END_OF_REPLAY_FILE_MARKER
from elma.constants import TOP10_MULTIPLAYER
from elma.constants import TOP10_SINGLEPLAYER
from elma.models import Frame
from elma.models import GroundTouchAEvent
from elma.models import GroundTouchBEvent
from elma.models import LeftVoltEvent
from elma.models import Level
from elma.models import Obj
from elma.models import ObjectTouchEvent
from elma.models import Picture
from elma.models import Point
from elma.models import Polygon
from elma.models import Replay
from elma.models import RightVoltEvent
from elma.models import TurnEvent
from elma.elma2 import Obj2
from elma.elma2 import Polygon2
from elma.elma2 import Line
from elma.elma2 import Path
from elma.elma2 import Platform
from elma.elma2 import Level2
from elma.elma2 import Frame2
from elma.elma2 import Event2
from elma.elma2 import Replay2
from elma.utils import null_padded
import random
import struct

try:
    bytes('A', 'latin1')
    PY_VERSION = 3
except TypeError:
    PY_VERSION = 2
    bytes = lambda a, b: a  # noqa
    _chr = chr
    chr = lambda a: a if type(a) == str else _chr(a) # noqa

packers = {
    'Point': lambda point:
        struct.pack('d', point.x) + struct.pack('d', point.y),
    'Polygon': lambda polygon: b''.join([
        struct.pack('I', polygon.grass),
        struct.pack('I', len(polygon.points)),
    ] + [pack_level(point) for point in polygon.points]),
    'Obj': lambda obj: b''.join([
        pack_level(obj.point),
        struct.pack('I', obj.type),
        struct.pack('I', obj.gravity),
        struct.pack('I', obj.animation_number)]),
    'Picture': lambda picture: b''.join([
        null_padded(picture.picture_name, 10),
        null_padded(picture.texture_name, 10),
        null_padded(picture.mask_name, 10),
        pack_level(picture.point),
        struct.pack('I', picture.distance),
        struct.pack('I', picture.clipping)])
}


def pack_level(item):
    """
    Pack a level-related item to its binary representation readable by
    Elastomania.
    """

    if type(item).__name__ in packers:
        return packers[type(item).__name__](item)

    level = item
    polygon_checksum = sum([sum([point.x + point.y
                                 for point in polygon.points])
                            for polygon in level.polygons])
    object_checksum = sum([obj.point.x + obj.point.y + obj.type
                           for obj in level.objects])
    picture_checksum = sum([picture.point.x + picture.point.y
                            for picture in level.pictures])
    collected_checksum = 3247.764325643 * (polygon_checksum +
                                           object_checksum +
                                           picture_checksum)
    integrity_1 = collected_checksum
    integrity_2 = random.randint(0, 5871) + 11877 - collected_checksum
    integrity_3 = random.randint(0, 5871) + 11877 - collected_checksum
    integrity_4 = random.randint(0, 6102) + 12112 - collected_checksum
    assert ((integrity_3 - integrity_2) <= 5871)

    return b''.join([
        bytes('POT14', 'latin1'),
        struct.pack('H', level.level_id & 0xFFFF),
        struct.pack('I', level.level_id),
        struct.pack('d', integrity_1),
        struct.pack('d', integrity_2),
        struct.pack('d', integrity_3),
        struct.pack('d', integrity_4),
        null_padded(level.name, 51),
        null_padded(level.lgr, 16),
        null_padded(level.ground_texture, 10),
        null_padded(level.sky_texture, 10),
        struct.pack('d', len(level.polygons) + 0.4643643),
    ] + [pack_level(polygon) for polygon in level.polygons] + [
        struct.pack('d', len(level.objects) + 0.4643643),
    ] + [pack_level(obj) for obj in level.objects] + [
        struct.pack('d', len(level.pictures) + 0.2345672),
    ] + [pack_level(picture) for picture in level.pictures] + [
        struct.pack('I', END_OF_DATA_MARKER),
    ] + [bytes(chr(c), 'latin1') for c in TOP10_SINGLEPLAYER] + [
    ] + [bytes(chr(c), 'latin1') for c in TOP10_MULTIPLAYER] + [
        struct.pack('I', END_OF_FILE_MARKER),
    ])


def unpack_level(data):
    """
    Unpack a level-related item from its binary representation readable by
    Elastomania.
    """

    data = iter(data)

    def munch(n):
        return b''.join([bytes(chr(next(data)), 'latin1') for _ in range(n)])

    assert munch(3) == b'POT'
    version = munch(2)
    if(version==b'14'):
        level = Level()
        munch(2)
        level.level_id = struct.unpack('I', munch(4))[0]
        munch(8 * 4)
        level.name = munch(51).rstrip(b'\0').decode('latin1')
        level.lgr = munch(16).rstrip(b'\0').decode('latin1')
        level.ground_texture = munch(10).rstrip(b'\0').decode('latin1')
        level.sky_texture = munch(10).rstrip(b'\0').decode('latin1')

        number_of_polygons = int(struct.unpack('d', munch(8))[0])
        for _ in range(number_of_polygons):
            grass = struct.unpack('I', munch(4))[0]
            number_of_vertices = struct.unpack('I', munch(4))[0]
            points = []
            for __ in range(number_of_vertices):
                x = struct.unpack('d', munch(8))[0]
                y = struct.unpack('d', munch(8))[0]
                points.append(Point(x, y))
            level.polygons.append(Polygon(points, grass=grass))

        number_of_objects = int(struct.unpack('d', munch(8))[0])
        for _ in range(number_of_objects):
            x = struct.unpack('d', munch(8))[0]
            y = struct.unpack('d', munch(8))[0]
            object_type = struct.unpack('I', munch(4))[0]
            gravity = struct.unpack('I', munch(4))[0]
            animation_number = struct.unpack('I', munch(4))[0]
            level.objects.append(Obj(Point(x, y),
                                     object_type,
                                     gravity=gravity,
                                     animation_number=animation_number))

        number_of_pictures = int(struct.unpack('d', munch(8))[0])
        for _ in range(number_of_pictures):
            picture_name = munch(10).rstrip(b'\0').decode('latin1')
            texture_name = munch(10).rstrip(b'\0').decode('latin1')
            mask_name = munch(10).rstrip(b'\0').decode('latin1')
            x = struct.unpack('d', munch(8))[0]
            y = struct.unpack('d', munch(8))[0]
            distance = struct.unpack('I', munch(4))[0]
            clipping = struct.unpack('I', munch(4))[0]
            level.pictures.append(Picture(Point(x, y),
                                          picture_name=picture_name,
                                          texture_name=texture_name,
                                          mask_name=mask_name,
                                          distance=distance,
                                          clipping=clipping))
        return level
    elif(version==b'35'):
        level = Level2()
        level.locked = False if struct.unpack('I', munch(4))[0] == 30 else True
        level.topology_error = True if struct.unpack('H', munch(2))[0] == 1 else False
        level.face_right = True if struct.unpack('I', munch(4))[0] == 1 else False
        level.name = munch(51).rstrip(b'\0').decode('latin1')
        level.lgr = munch(16).rstrip(b'\0').decode('latin1')
        munch(10) #ground texture name
        munch(10) #sky texture name
        munch(20) #?unused editable
        munch(4) #shareware flag
        
        number_of_polygons = int(struct.unpack('d', munch(8))[0])
        for _ in range(number_of_polygons):
            polygon = Polygon2()
            polygon.type = struct.unpack('I', munch(4))[0]
            polygon.path_sub_type  = struct.unpack('I', munch(4))[0]
            polygon.platform_id = struct.unpack('I', munch(4))[0]
            polygon.path_id = struct.unpack('I', munch(4))[0]
            polygon.path_speed_at_top = struct.unpack('I', munch(4))[0]
            polygon.path_speed_at_bottom = struct.unpack('I', munch(4))[0]
            polygon.path_open_delay = struct.unpack('I', munch(4))[0]
            polygon.path_close_delay = struct.unpack('I', munch(4))[0]
            polygon.path_reopen_delay = struct.unpack('I', munch(4))[0]
            polygon.path_reclose_delay = struct.unpack('I', munch(4))[0]
            polygon.path_initially = struct.unpack('I', munch(4))[0]
            polygon.path_no_of_platforms = struct.unpack('I', munch(4))[0]
            polygon.override_force_solid = struct.unpack('I', munch(4))[0]
            polygon.style_index = struct.unpack('I', munch(4))[0]
            polygon.override_border = struct.unpack('I', munch(4))[0] + struct.unpack('I', munch(4))[0]*2 + struct.unpack('I', munch(4))[0]*3
            polygon.override_distance = struct.unpack('I', munch(4))[0]
            munch(4)
            polygon.comments = munch(300).rstrip(b'\0').decode('latin1')
            
            number_of_vertices = struct.unpack('I', munch(4))[0]
            for __ in range(number_of_vertices):
                x = struct.unpack('d', munch(8))[0]
                y = struct.unpack('d', munch(8))[0]
                polygon.points.append(Point(x, y))
            level.polygons.append(polygon)
        
        if(not level.topology_error):
            number_of_lines = struct.unpack('I', munch(4))[0]
            for _ in range(number_of_lines):
                x = struct.unpack('d', munch(8))[0]
                y = struct.unpack('d', munch(8))[0]
                dx=struct.unpack('d', munch(8))[0]
                dy=struct.unpack('d', munch(8))[0]
                x2 = x+dx
                y2 = y+dy
                dx_unit=struct.unpack('d', munch(8))[0]
                dy_unit=struct.unpack('d', munch(8))[0]
                length=struct.unpack('d', munch(8))[0]
                assert(abs(dx_unit*dx_unit+dy_unit*dy_unit-1)<0.00001)
                assert(abs(length*length-dx*dx-dy*dy)<0.00001)
                assert(abs(dx_unit-dx/length)<0.00001)
                assert(abs(dy_unit-dy/length)<0.00001)
                texture_id=struct.unpack('I', munch(4))[0]
                unsolid=True if struct.unpack('I', munch(4))[0] == 1 else False
                distance=struct.unpack('I', munch(4))[0]
                invisible=True if struct.unpack('I', munch(4))[0] == 1 else False
                level.lines.append(Line(Point(x,y),Point(x2,y2),texture_id,unsolid,distance,invisible))
            
            level.unknown1 = struct.unpack('d', munch(8))[0]
            level.level_id = struct.unpack('I', munch(4))[0]
            
            while struct.unpack('B', munch(1))[0]==1:
                platform = Platform()
                xmin05=struct.unpack('d', munch(8))[0]
                ymin05=struct.unpack('d', munch(8))[0]
                xmin=struct.unpack('d', munch(8))[0]
                ymin=struct.unpack('d', munch(8))[0]
                xmax=struct.unpack('d', munch(8))[0]
                ymax=struct.unpack('d', munch(8))[0]
                xmin33=struct.unpack('d', munch(8))[0]
                ymin33=struct.unpack('d', munch(8))[0]
                xmin2=struct.unpack('d', munch(8))[0]
                ymin2=struct.unpack('d', munch(8))[0]
                xmax2=struct.unpack('d', munch(8))[0]
                ymax2=struct.unpack('d', munch(8))[0]
                objectx=struct.unpack('d', munch(8))[0]
                objecty=struct.unpack('d', munch(8))[0]
                platform.platform_reference_point = Point(objectx,objecty)
                assert(abs(xmin-xmin2)<0.00001)
                assert(abs(ymin-ymin2)<0.00001)
                assert(abs(xmax-xmax2)<0.00001)
                assert(abs(ymax-ymax2)<0.00001)
                assert(abs(xmin-xmin05-0.05)<0.00001)
                assert(abs(xmin-xmin33-0.33)<0.00001)
                assert(abs(ymin-ymin05-0.05)<0.00001)
                assert(abs(ymin-ymin33-0.33)<0.00001)
                
                platform.int112=struct.unpack('I', munch(4))[0]
                platform.int116=struct.unpack('I', munch(4))[0]
                platform.int120=struct.unpack('I', munch(4))[0]
                platform.int124=struct.unpack('I', munch(4))[0]
                platform.platform_id=struct.unpack('I', munch(4))[0]
                platform.int132=struct.unpack('I', munch(4))[0]
                platform.int136=struct.unpack('I', munch(4))[0]
                platform.int140=struct.unpack('I', munch(4))[0]
                platform.int144=struct.unpack('I', munch(4))[0]
                platform.int148=struct.unpack('I', munch(4))[0]
                platform.int152=struct.unpack('I', munch(4))[0]
                platform.int156=struct.unpack('I', munch(4))[0]
                platform.texture_id=struct.unpack('I', munch(4))[0]
                platform.border_texture_id=struct.unpack('I', munch(4))[0]
                assert(struct.unpack('I', munch(4))[0]==1478256389)
                assert(struct.unpack('I', munch(4))[0]==0)
                total_lines_in_platform=struct.unpack('I', munch(4))[0]
                for _ in range(total_lines_in_platform):
                    x = struct.unpack('d', munch(8))[0]
                    y = struct.unpack('d', munch(8))[0]
                    dx=struct.unpack('d', munch(8))[0]
                    dy=struct.unpack('d', munch(8))[0]
                    x2 = x+dx
                    y2 = y+dy
                    dx_unit=struct.unpack('d', munch(8))[0]
                    dy_unit=struct.unpack('d', munch(8))[0]
                    length=struct.unpack('d', munch(8))[0]
                    assert(abs(dx_unit*dx_unit+dy_unit*dy_unit-1)<0.00001)
                    assert(abs(length*length-dx*dx-dy*dy)<0.00001)
                    assert(abs(dx_unit-dx/length)<0.00001)
                    assert(abs(dy_unit-dy/length)<0.00001)
                    texture_id=struct.unpack('I', munch(4))[0]
                    solid=True if struct.unpack('I', munch(4))[0] == 1 else False
                    distance=struct.unpack('I', munch(4))[0]
                    invisible=True if struct.unpack('I', munch(4))[0] == 1 else False
                    assert(texture_id==0)
                    assert(solid==False)
                    assert(distance==0)
                    assert(invisible==False)
                    platform.lines.append(Line(Point(x,y),Point(x2,y2),texture_id,solid,distance,invisible))
                level.platforms.append(platform)
                
            assert munch(1)==b'\x58'
            level.unknown2=struct.unpack('d', munch(8))[0]
            level.unknown3=struct.unpack('d', munch(8))[0]
            
            while struct.unpack('B', munch(1))[0]==1:
                path=Path()
                assert(len(munch(24).rstrip(b'\0'))==0)
                path.path_id=struct.unpack('I',munch(4))[0]
                path.path_platform_id=struct.unpack('I',munch(4))[0]
                path.path_sub_type=struct.unpack('I',munch(4))[0]
                path.path_speed_at_top=struct.unpack('I',munch(4))[0]
                path.path_speed_at_bottom=struct.unpack('I',munch(4))[0]
                path.path_open_delay=struct.unpack('I',munch(4))[0]
                path.path_close_delay=struct.unpack('I',munch(4))[0]
                path.path_reopen_delay=struct.unpack('I',munch(4))[0]
                path.path_reclose_delay=struct.unpack('I',munch(4))[0]
                path.path_initially=struct.unpack('I',munch(4))[0]
                path.path_no_of_platforms=struct.unpack('I',munch(4))[0]
                assert(len(munch(16).rstrip(b'\0'))==0)
                path.forward_frames=struct.unpack('I',munch(4))[0]
                path.total_frames=struct.unpack('I',munch(4))[0]
                assert(len(munch(4).rstrip(b'\0'))==0)
                path.int96=struct.unpack('I',munch(4))[0]
                path.int100=struct.unpack('I',munch(4))[0]
                path.int104=struct.unpack('I',munch(4))[0]
                path.int108=struct.unpack('I',munch(4))[0]
                path.int112=struct.unpack('I',munch(4))[0]
                assert(len(munch(20).rstrip(b'\0'))==0)
                path.int136=struct.unpack('I',munch(4))[0]
                assert(path.int136==struct.unpack('I',munch(4))[0])
                assert(path.int136==struct.unpack('I',munch(4))[0])
                assert(path.int136==struct.unpack('I',munch(4))[0])
                assert(path.int136==struct.unpack('I',munch(4))[0])
                assert(path.int136==struct.unpack('I',munch(4))[0])
                assert(struct.unpack('I', munch(4))[0]==1236278137)
                assert(len(munch(4).rstrip(b'\0'))==0)
                
                for i in range(path.total_frames):
                    path.movement_x.append(struct.unpack('i', munch(4))[0])
                assert(munch(1)==b'\x15')
                for i in range(path.total_frames):
                    path.movement_y.append(struct.unpack('i', munch(4))[0])
                assert(munch(1)==b'\x15')
                for i in range(path.total_frames):
                    path.velocity_x.append(struct.unpack('i', munch(4))[0])
                assert(munch(1)==b'\x15')
                for i in range(path.total_frames):
                    path.velocity_y.append(struct.unpack('i', munch(4))[0])
                assert(munch(1)==b'\x15')
                for i in range(path.total_frames):
                    path.platform_x.append(struct.unpack('i', munch(4))[0])
                assert(munch(1)==b'\x15')
                for i in range(path.total_frames):
                    path.platform_y.append(struct.unpack('i', munch(4))[0])
                assert(munch(1)==b'\x15')
                level.paths.append(path)
                
        assert munch(1) == b'\x2B'
        assert struct.unpack('I', munch(4))[0]==192356446
        
        number_of_objects = int(struct.unpack('d', munch(8))[0])
        for _ in range(number_of_objects):
            x = struct.unpack('d', munch(8))[0]
            y = struct.unpack('d', munch(8))[0]
            type = struct.unpack('I', munch(4))[0]
            group_id  = struct.unpack('I', munch(4))[0]
            path_id = struct.unpack('I', munch(4))[0]
            opener_closer = struct.unpack('I', munch(4))[0]
            card = struct.unpack('I', munch(4))[0]
            condition_state = struct.unpack('I', munch(4))[0]
            platform_id = struct.unpack('I', munch(4))[0]
            comments = munch(300).rstrip(b'\0').decode('latin1')
            level.objects.append(Obj2(Point(x,y), type, group_id, path_id, 
                                      opener_closer, card, condition_state,
                                      platform_id, comments))

        number_of_pictures = int(struct.unpack('d', munch(8))[0])
        for _ in range(number_of_pictures):
            picture_name = munch(10).rstrip(b'\0').decode('latin1')
            texture_name = munch(10).rstrip(b'\0').decode('latin1')
            mask_name = munch(10).rstrip(b'\0').decode('latin1')
            x = struct.unpack('d', munch(8))[0]
            y = struct.unpack('d', munch(8))[0]
            distance = struct.unpack('I', munch(4))[0]
            clipping = struct.unpack('I', munch(4))[0]
            level.pictures.append(Picture(Point(x, y),
                                          picture_name=picture_name,
                                          texture_name=texture_name,
                                          mask_name=mask_name,
                                          distance=distance,
                                          clipping=clipping))
        return level
    return None


def unpack_replay(data):
    """
    Unpack a replay-related item from its binary representation readable by
    Elastomania.
    """

    data = iter(data)

    def munch(n):
        return b''.join([bytes(chr(next(data)), 'latin1') for _ in range(n)])

    def read_int32():
        return struct.unpack('i', munch(4))[0]

    def read_uint32():
        return struct.unpack('I', munch(4))[0]

    def read_int16():
        return struct.unpack('h', munch(2))[0]

    def read_uint8():
        return struct.unpack('B', munch(1))[0]

    def read_float():
        return struct.unpack('f', munch(4))[0]

    def read_double():
        return struct.unpack('d', munch(8))[0]

    header = read_int32()
    
    if(header!=555365777):
        replay = Replay()

        number_of_replay_frames = header
        munch(4)
        replay.is_multi = bool(read_int32())
        replay.is_flagtag = bool(read_int32())
        replay.level_id = read_uint32()
        replay.level_name = munch(12).rstrip(b'\0').decode('latin1')
        munch(4)

        frame_numbers = range(number_of_replay_frames)
        xs = [read_float() for _ in frame_numbers]
        ys = [read_float() for _ in frame_numbers]
        left_wheel_xs = [read_int16() for _ in frame_numbers]
        left_wheel_ys = [read_int16() for _ in frame_numbers]
        right_wheel_xs = [read_int16() for _ in frame_numbers]
        right_wheel_ys = [read_int16() for _ in frame_numbers]
        head_xs = [read_int16() for _ in frame_numbers]
        head_ys = [read_int16() for _ in frame_numbers]
        rotations = [read_int16() for _ in frame_numbers]
        left_wheel_rotations = [read_uint8() for _ in frame_numbers]
        right_wheel_rotations = [read_uint8() for _ in frame_numbers]
        gas_and_turn_states = [read_uint8() for _ in frame_numbers]
        sound_effect_volumes = [read_int16() for _ in frame_numbers]

        for (x,
             y,
             left_wheel_x,
             left_wheel_y,
             right_wheel_x,
             right_wheel_y,
             head_x,
             head_y,
             rotation,
             left_wheel_rotation,
             right_wheel_rotation,
             gas_and_turn_state,
             sound_effect_volume) in zip(xs,
                                         ys,
                                         left_wheel_xs,
                                         left_wheel_ys,
                                         right_wheel_xs,
                                         right_wheel_ys,
                                         head_xs,
                                         head_ys,
                                         rotations,
                                         left_wheel_rotations,
                                         right_wheel_rotations,
                                         gas_and_turn_states,
                                         sound_effect_volumes):

            frame = Frame()
            frame.position = Point(x, y)
            frame.left_wheel_position = Point(left_wheel_x, left_wheel_y)
            frame.right_wheel_position = Point(right_wheel_x, right_wheel_y)
            frame.head_position = Point(head_x, head_y)
            frame.rotation = rotation
            frame.left_wheel_rotation = left_wheel_rotation
            frame.right_wheel_rotation = right_wheel_rotation
            frame.is_gasing = bool(gas_and_turn_state & 0b1)
            frame.is_turned_right = bool(gas_and_turn_state & 0b10)
            frame.player1_has_flag = bool(gas_and_turn_state & 0b100)
            frame.flag_holder_losing_flag = bool(gas_and_turn_state & 0b1000)
            frame._gas_and_turn_state = gas_and_turn_state #preserve remaining 4 bits of state
                #As per Smibu this should correspond to (the lowest byte of the y-coordinate of the center of the bike) & 0xF0 but I have not found this to be the case
            frame.spring_sound_effect_volume = sound_effect_volume
            replay.frames.append(frame)

        number_of_replay_events = read_int32()
        for _ in range(number_of_replay_events):
            event_time = read_double()
            event_type_1 = read_int32()
            event_type_2 = read_int32()

            if event_type_2 == 0:
                event = ObjectTouchEvent()
                event.object_number = event_type_1
            elif event_type_1 == 393215 and event_type_2 == 1065185444:
                event = TurnEvent()
            elif event_type_1 == 524287 and event_type_2 == 1065185444:
                event = LeftVoltEvent()
            elif event_type_1 == 458751 and event_type_2 == 1065185444:
                event = RightVoltEvent()
            elif event_type_1 == 131071:
                event = GroundTouchAEvent()
                event.value = event_type_2
            elif event_type_1 == 327679:
                event = GroundTouchBEvent()
                event.value = event_type_2

            event.time = event_time
            replay.events.append(event)
        
        assert read_uint32() == END_OF_REPLAY_FILE_MARKER
        
        if(replay.is_multi):
            number_of_replay_frames = read_int32()
            munch(4+4+4+4+12+4)

            frame_numbers = range(number_of_replay_frames)
            xs = [read_float() for _ in frame_numbers]
            ys = [read_float() for _ in frame_numbers]
            left_wheel_xs = [read_int16() for _ in frame_numbers]
            left_wheel_ys = [read_int16() for _ in frame_numbers]
            right_wheel_xs = [read_int16() for _ in frame_numbers]
            right_wheel_ys = [read_int16() for _ in frame_numbers]
            head_xs = [read_int16() for _ in frame_numbers]
            head_ys = [read_int16() for _ in frame_numbers]
            rotations = [read_int16() for _ in frame_numbers]
            left_wheel_rotations = [read_uint8() for _ in frame_numbers]
            right_wheel_rotations = [read_uint8() for _ in frame_numbers]
            gas_and_turn_states = [read_uint8() for _ in frame_numbers]
            sound_effect_volumes = [read_int16() for _ in frame_numbers]

            for (x,
                 y,
                 left_wheel_x,
                 left_wheel_y,
                 right_wheel_x,
                 right_wheel_y,
                 head_x,
                 head_y,
                 rotation,
                 left_wheel_rotation,
                 right_wheel_rotation,
                 gas_and_turn_state,
                 sound_effect_volume) in zip(xs,
                                             ys,
                                             left_wheel_xs,
                                             left_wheel_ys,
                                             right_wheel_xs,
                                             right_wheel_ys,
                                             head_xs,
                                             head_ys,
                                             rotations,
                                             left_wheel_rotations,
                                             right_wheel_rotations,
                                             gas_and_turn_states,
                                             sound_effect_volumes):

                frame = Frame()
                frame.position = Point(x, y)
                frame.left_wheel_position = Point(left_wheel_x, left_wheel_y)
                frame.right_wheel_position = Point(right_wheel_x, right_wheel_y)
                frame.head_position = Point(head_x, head_y)
                frame.rotation = rotation
                frame.left_wheel_rotation = left_wheel_rotation
                frame.right_wheel_rotation = right_wheel_rotation
                frame.is_gasing = bool(gas_and_turn_state & 0b1)
                frame.is_turned_right = bool(gas_and_turn_state & 0b10)
                frame.player1_has_flag = bool(gas_and_turn_state & 0b100)
                frame.flag_holder_losing_flag = bool(gas_and_turn_state & 0b1000)
                frame._gas_and_turn_state = gas_and_turn_state #preserve remaining 4 bits of state
                    #As per Smibu this should correspond to (the lowest byte of the y-coordinate of the center of the bike) & 0xF0 but I have not found this to be the case
                frame.spring_sound_effect_volume = sound_effect_volume
                replay.frames2.append(frame)

            number_of_replay_events = read_int32()
            for _ in range(number_of_replay_events):
                event_time = read_double()
                event_type_1 = read_int32()
                event_type_2 = read_int32()

                if event_type_2 == 0:
                    event = ObjectTouchEvent()
                    event.object_number = event_type_1
                elif event_type_1 == 393215 and event_type_2 == 1065185444:
                    event = TurnEvent()
                elif event_type_1 == 524287 and event_type_2 == 1065185444:
                    event = LeftVoltEvent()
                elif event_type_1 == 458751 and event_type_2 == 1065185444:
                    event = RightVoltEvent()
                elif event_type_1 == 131071:
                    event = GroundTouchAEvent()
                    event.value = event_type_2
                elif event_type_1 == 327679:
                    event = GroundTouchBEvent()
                    event.value = event_type_2

                event.time = event_time
                replay.events2.append(event)
            assert read_uint32() == END_OF_REPLAY_FILE_MARKER
        return replay
    else:
        replay = Replay2()
        assert read_int32() == 0x66
        replay.restore_points_used = bool(read_int32())
        assert read_int32() == 0x2C
        replay.level_id = read_int32()
        replay.is_internal = bool(read_int32())
        replay.pack_id = read_int32()
        replay.pack_level_number = read_int32()
        replay.level_name = munch(28).rstrip(b'\0').decode('latin1')
        frame_n = read_int32()
        event_n = read_int32()
        for _ in range(frame_n):
            frame = Frame2()
            x = read_float()
            y = read_float()
            left_wheel_x = read_int16()
            left_wheel_y = read_int16()
            right_wheel_x = read_int16()
            right_wheel_y = read_int16()
            head_x = read_int16()
            head_y = read_int16()
            rotation = read_int16()
            rotation2 = read_int16()
            rotation3 = read_int16()
            rotation4 = read_int16()
            upcount = read_int32()
            frame.position = Point(x, y)
            frame.left_wheel_position = Point(left_wheel_x, left_wheel_y)
            frame.right_wheel_position = Point(right_wheel_x, right_wheel_y)
            frame.head_position = Point(head_x, head_y)
            frame.left_wheel_rotation = rotation
            frame.right_wheel_rotation = rotation2
            frame.bike_rotation = rotation3
            frame.back_wheel_rotation_speed = rotation4
            frame.back_wheel_air_time = upcount
            replay.frames.append(frame)
        for _ in range(event_n):
            event = Event2()
            event.frame_of_event = read_int32()
            event.event_id = read_int32()
            event.object_id = read_int32()
            assert read_int32() == 0
            replay.events.append(event)
        assert read_int32() == 555365777
        return replay

def pack_replay(item):
    """
    Pack a replay-related item to its binary representation readable by
    Elastomania.
    """

    if isinstance(item, ObjectTouchEvent):
        return (struct.pack('d', item.time) +
                struct.pack('I', item.object_number) +
                struct.pack('I', 0))

    if isinstance(item, TurnEvent):
        return (struct.pack('d', item.time) +
                struct.pack('I', 393215) +
                struct.pack('I', 1065185444))

    if isinstance(item, LeftVoltEvent):
        return (struct.pack('d', item.time) +
                struct.pack('I', 524287) +
                struct.pack('I', 1065185444))

    if isinstance(item, RightVoltEvent):
        return (struct.pack('d', item.time) +
                struct.pack('I', 458751) +
                struct.pack('I', 1065185444))

    if isinstance(item, GroundTouchAEvent):
        return (struct.pack('d', item.time) +
                struct.pack('I', 131071) +
                struct.pack('I', item.value))

    if isinstance(item, GroundTouchBEvent):
        return (struct.pack('d', item.time) +
                struct.pack('I', 327679) +
                struct.pack('I', item.value))

    replay = item
    file = b''.join([
        struct.pack('i', len(replay.frames)),
        struct.pack('i', 0x83),
        struct.pack('i', replay.is_multi),
        struct.pack('i', replay.is_flagtag),
        struct.pack('I', replay.level_id),
        null_padded(replay.level_name, 12),
        struct.pack('i', 0),
        b''.join([struct.pack('f', frame.position.x)
                  for frame in replay.frames]),
        b''.join([struct.pack('f', frame.position.y)
                  for frame in replay.frames]),
        b''.join([struct.pack('h', frame.left_wheel_position.x)
                  for frame in replay.frames]),
        b''.join([struct.pack('h', frame.left_wheel_position.y)
                  for frame in replay.frames]),
        b''.join([struct.pack('h', frame.right_wheel_position.x)
                  for frame in replay.frames]),
        b''.join([struct.pack('h', frame.right_wheel_position.y)
                  for frame in replay.frames]),
        b''.join([struct.pack('h', frame.head_position.x)
                  for frame in replay.frames]),
        b''.join([struct.pack('h', frame.head_position.y)
                  for frame in replay.frames]),
        b''.join([struct.pack('h', frame.rotation)
                  for frame in replay.frames]),
        b''.join([struct.pack('B', frame.left_wheel_rotation)
                  for frame in replay.frames]),
        b''.join([struct.pack('B', frame.right_wheel_rotation)
                  for frame in replay.frames]),
        b''.join([struct.pack('B',
                              frame._gas_and_turn_state & 0xF0 | (frame.flag_holder_losing_flag << 3) | (frame.player1_has_flag << 2) |
                              (frame.is_turned_right << 1) | frame.is_gasing)
                 for frame in replay.frames]),
        b''.join([struct.pack('h', frame.spring_sound_effect_volume)
                  for frame in replay.frames]),
        struct.pack('I', len(replay.events)),
        b''.join([pack_replay(event) for event in replay.events]),
        struct.pack('I', END_OF_REPLAY_FILE_MARKER),
    ])
    if(replay.is_multi):
        file = b''.join([file,
            struct.pack('i', len(replay.frames2)),
            struct.pack('i', 0x83),
            struct.pack('i', replay.is_multi),
            struct.pack('i', replay.is_flagtag),
            struct.pack('I', replay.level_id),
            null_padded(replay.level_name, 12),
            struct.pack('i', 0),
            b''.join([struct.pack('f', frame.position.x)
                      for frame in replay.frames2]),
            b''.join([struct.pack('f', frame.position.y)
                      for frame in replay.frames2]),
            b''.join([struct.pack('h', frame.left_wheel_position.x)
                      for frame in replay.frames2]),
            b''.join([struct.pack('h', frame.left_wheel_position.y)
                      for frame in replay.frames2]),
            b''.join([struct.pack('h', frame.right_wheel_position.x)
                      for frame in replay.frames2]),
            b''.join([struct.pack('h', frame.right_wheel_position.y)
                      for frame in replay.frames2]),
            b''.join([struct.pack('h', frame.head_position.x)
                      for frame in replay.frames2]),
            b''.join([struct.pack('h', frame.head_position.y)
                      for frame in replay.frames2]),
            b''.join([struct.pack('h', frame.rotation)
                      for frame in replay.frames2]),
            b''.join([struct.pack('B', frame.left_wheel_rotation)
                      for frame in replay.frames2]),
            b''.join([struct.pack('B', frame.right_wheel_rotation)
                      for frame in replay.frames2]),
            b''.join([struct.pack('B',
                                  frame._gas_and_turn_state & 0xF0 | (frame.flag_holder_losing_flag << 3) | (frame.player1_has_flag << 2) |
                                  (frame.is_turned_right << 1) | frame.is_gasing)
                     for frame in replay.frames2]),
            b''.join([struct.pack('h', frame.spring_sound_effect_volume)
                      for frame in replay.frames2]),
            struct.pack('I', len(replay.events2)),
            b''.join([pack_replay(event) for event in replay.events2]),
            struct.pack('I', END_OF_REPLAY_FILE_MARKER),
            ])
    return file
