from elma.packing import unpack_level
from elma.elma2 import pack_level2

process=1

def Chop(path,x):
    path.movement_x=path.movement_x[:x]
    path.movement_y=path.movement_y[:x]
    path.velocity_x=path.velocity_x[:x]
    path.velocity_y=path.velocity_y[:x]
    path.platform_x=path.platform_x[:x]
    path.platform_y=path.platform_y[:x]
    return
def WeaveAll(path):
    path.movement_x = WeavePlus(path.movement_x)
    path.movement_y = WeavePlus(path.movement_y)
    path.velocity_x = WeavePlus(path.velocity_x)
    path.velocity_y = WeavePlus(path.velocity_y)
    path.platform_x = WeavePlus(path.platform_x)
    path.platform_y = WeavePlus(path.platform_y)
def WeavePlus(array):
    length=len(array)
    i=0
    newarray=[]
    while i<length:
        if(length-i == 1):
            newarray.append(array[i])
        else:
            newarray.append(array[i]+array[i+1])
        i+=2
    return newarray
def WeaveWeakAll(path):
    path.movement_x = WeaveWeak(path.movement_x)
    path.movement_y = WeaveWeak(path.movement_y)
    path.velocity_x = WeaveWeak(path.velocity_x)
    path.velocity_y = WeaveWeak(path.velocity_y)
    path.platform_x = WeaveWeak(path.platform_x)
    path.platform_y = WeaveWeak(path.platform_y)
def WeaveWeak(array):
    length=len(array)
    i=0
    newarray=[]
    while i<length:
        newarray.append(array[i])
        i+=2
    return newarray
def DuplicateAll(path):
    framereference=path.forward_frames
    #path.movement_x = Dup(path.movement_x,framereference)
    #path.movement_y = Dup(path.movement_y,framereference)
    path.velocity_x = Dup(path.velocity_x,framereference)
    path.velocity_y = Dup(path.velocity_y,framereference)
    #path.platform_x = Dup(path.platform_x,framereference)
    #path.platform_y = Dup(path.platform_y,framereference)
def Dup(array,i_ref):
    length=len(array)
    i=0
    while i<i_ref:
        array[i_ref+i]=array[i]
        i+=1
    return array
    
def Process(level):
    for path in level.paths:
        if(process==0):
            path.forward_frames=path.forward_frames//2
            path.total_frames=path.total_frames//2
            print(path.total_frames)
            Chop(path,path.total_frames)
        if(process==1):
            WeaveAll(path)
            path.forward_frames=path.forward_frames//2
            path.total_frames=len(path.movement_x)
        if(process==2):
            WeaveWeakAll(path)
            path.forward_frames=path.forward_frames//2
            path.total_frames=len(path.movement_x)
        

with open('imptest.lev','rb') as f:
    level = unpack_level(f.read())
    #Process(level)
    with open('C:\\Users\\Public\\Documents\\ElastoMania\\elma_ii_registered_101\\lev\\aaat.lev','wb') as g:
        g.write(pack_level2(level))
    
with open('impsa02.lev','rb') as f:
    level = unpack_level(f.read())
    #Process(level)
    with open('C:\\Users\\Public\\Documents\\ElastoMania\\elma_ii_registered_101\\lev\\aaa2.lev','wb') as g:
        g.write(pack_level2(level))
with open('impsyflower.lev','rb') as f:
    level = unpack_level(f.read())
    #Process(level)
    with open('C:\\Users\\Public\\Documents\\ElastoMania\\elma_ii_registered_101\\lev\\aaa2.lev','wb') as g:
        g.write(pack_level2(level))
        
with open('C:\\Users\\Public\\Documents\\ElastoMania\\elma_ii_registered_101\\lev\\noob.lev','rb') as f:
    level = unpack_level(f.read())
    for path in level.paths:
        path.velocity_x[0]=10000*path.path_id
        path.velocity_y[0]=-10000
    #path=level.paths[0]
    #print(level.paths[0].forward_frames)
    #print(level.paths[0].total_frames)
    #print(level.paths[0].movement_x[:level.paths[0].forward_frames])
    #print(level.paths[0].movement_x[level.paths[0].forward_frames:])
    #print(level.paths[0].velocity_x[:level.paths[0].forward_frames])
    #print(level.paths[0].platform_x[:level.paths[0].forward_frames])
    #print(level.paths[0].velocity_y[:level.paths[0].forward_frames])
    #print(level.paths[0].velocity_y[level.paths[0].forward_frames:])
    #for i in range(1,path.total_frames):
    #    path.movement_x[i]=-1#(1)*((i%2)*2-1)
    #path.velocity_x[0]=20000
    #path.movement_x[2]=0
    #path.velocity_y[6]=1
    #path.velocity_y[7]=1
    #print(level.paths[0].velocity_y[:level.paths[0].forward_frames])
    with open('C:\\Users\\Public\\Documents\\ElastoMania\\elma_ii_registered_101\\lev\\aaan.lev','wb') as g:
        g.write(pack_level2(level))