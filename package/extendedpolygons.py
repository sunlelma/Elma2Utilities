from elma.packing import unpack_level
from elma.elma2 import pack_level2

import tkinter as tk
import tkinter.filedialog as filedialog
import os
import sys
import struct
import shutil
import time

class Application(tk.Frame):
    def __init__(self,master):
        super().__init__(master)
        self.pack()
        self.create_widgets()
        
    def create_widgets(self):
        self.frame1=tk.Frame(self)
        self.frame1.pack(side="top")
        self.button1=tk.Button(self.frame1,text="Choose .lev file",command=self.SelectPopup1)
        self.button1.pack(side="top")
        self.entry4=tk.Entry(self,width=80)
        self.entry4.insert(0,"lev path")
        self.entry4.pack(side="top")
        self.button2=tk.Button(self.frame1,text="Choose .txt",command=self.SelectPopup2)
        self.button2.pack(side="top")
        self.entry3=tk.Entry(self,width=80)
        self.entry3.insert(0,"txt path")
        self.entry3.pack(side="top")
        self.label8=tk.Label(self,text=" ")
        self.label8.pack(side="top")
        self.button6=tk.Button(self,text="Patch Extended Polygons***",command=self.OpenStuff,fg="red")
        self.button6.pack(side="top")
        
    def SelectPopup1(self):
        imgfile=filedialog.askopenfilename(filetypes=[("Lev file",("*.lev")),("All","*")])
        self.entry4.delete(0,len(self.entry4.get()))
        self.entry4.insert(0,imgfile)
    def SelectPopup2(self):
        imgfile=filedialog.askopenfilename(filetypes=[("Text file","*.txt"),("All","*")])
        self.entry3.delete(0,len(self.entry4.get()))
        self.entry3.insert(0,imgfile)

    def OpenStuff(self):
        exe=self.entry4.get()
        txt=self.entry3.get()
        if(exe[-4:].lower()!=".lev"):
            print("Not .lev! Aborting")
            return
        if not(os.path.exists("backup")):
            os.mkdir("backup")
            print("Made backup/ directory")
        backuppath="backup/%s_%s.lev.bak"%(os.path.basename(exe)[:-4],time.strftime("%Y%m%d%H%M%S",time.gmtime()))
        shutil.copy2(exe,backuppath)
        print("Saved backup file: %s"%backuppath)
        
        
        with open(txt,"r") as f:
            level=0
            with open(exe,"rb") as g:
                level = unpack_level(g.read())
                path_table = [None]*1000
                for path in level.paths:
                    path_table[path.path_id]=path
                commands=f.read().splitlines()
                for line in commands:
                    if(len(line)==0):
                        continue
                    
                    command=line.split(",")
                    if(len(command[0])==0):
                        continue
                    target_path_id=int(command[0])
                    if(path_table[target_path_id] is None):
                        print("Missing path! Skipping - %s"%line)
                        continue
                    assert(target_path_id>=0)
                    assert(target_path_id<=999)
                    if(command[1]=='passive_velocity_x'):
                        set_value=int(command[2])
                        path_table[target_path_id].velocity_x[0]=set_value
                    elif(command[1]=='passive_velocity_y'):
                        set_value=int(command[2])
                        path_table[target_path_id].velocity_y[0]=set_value
                    elif(command[1]=='null_velocity_x'):
                        for i in range(len(path_table[target_path_id].velocity_x)):
                            path_table[target_path_id].velocity_x[i]=0
                    elif(command[1]=='null_velocity_y'):
                        for i in range(len(path_table[target_path_id].velocity_y)):
                            path_table[target_path_id].velocity_y[i]=0
                            
                    else:
                        raise Exception("Unknown command: %s"%command[1])
            with open(exe,"wb") as g:
                g.write(pack_level2(level))
        print("Done! Extended polygons added!\n\n\n")
        return
        
root=tk.Tk()
root.title("sunl's Elma 2 Extended Polygons v1.00")
app=Application(master=root)

app.mainloop()