import os
import struct


num2obj=['Flower','Food','Killer','Start','Bonus','Card','Switch','Group switch','Platform reference','T_stage','T_fordit turn','T_ugrat jump','Teleport']
clipping2txt=['U','G','S']
offset=0

def output(output,msg,silent=True):
    if(silence!=True or silent!=True):
        output.write('%4X: %s'%(offset,msg))
        output.write('\n')
        print('%4X: %s'%(offset,msg))
    return
        
def LoopLevel():
    o=open("output.txt",'w')
    for file in os.listdir():
        #if(file.lower()=="corrupt.lev" or (allLevels==True and file.lower().endswith(".lev"))):
        if(file.lower()=="polywow.lev" or (allLevels==True and file.lower().endswith(".lev"))):
        #if(file.lower()=="CsabaIIpk1n16.lev".lower()):
            output(o,file,False)
            with open(file,'rb') as f:
                offset=0
                data = iter(f.read())

                def munch(n):
                    global offset
                    offset+=n
                    return b''.join([bytes(chr(next(data)), 'latin1') for _ in range(n)])
                assert munch(3) == b'POT'
                version=munch(2)
                output(o,'Version: %s'%version.decode('latin1'))
                output(o,'Unlocked' if struct.unpack('I', munch(4))[0]==30 else 'LOCKED')
                topo=struct.unpack('H', munch(2))[0]
                output(o,'TOPO ERROR' if topo==1 else 'No topology error')
                output(o,'TURN RIGHT AT START' if struct.unpack('I', munch(4))[0]==1 else 'Face left')
                output(o,'Title: %s'%munch(51).rstrip(b'\0').decode('latin1'))
                output(o,'Lgr: %s'%munch(16).rstrip(b'\0').decode('latin1'))
                output(o,'Ground: %s'%munch(10).rstrip(b'\0').decode('latin1'))
                output(o,'Sky: %s'%munch(10).rstrip(b'\0').decode('latin1'))
                assert(len(munch(20).rstrip(b'\0'))==0)
                sharewareCheck=struct.unpack('I', munch(4))[0]
                output(o,'Shareware level' if sharewareCheck==115 else 'Shareware check: %s'%sharewareCheck)
                number_of_polygons = int(struct.unpack('d', munch(8))[0])
                output(o,'')
                output(o,'Poly#: %s (y flipped for consistency)'%number_of_polygons)
                for _ in range(number_of_polygons):
                    p_type=struct.unpack('I',munch(4))[0]
                    if(p_type==1):
                        munch(4*11)
                        override1=struct.unpack('I', munch(4))[0]
                        style=struct.unpack('I', munch(4))[0]
                        override2=struct.unpack('I', munch(4))[0]
                        override3=struct.unpack('I', munch(4))[0]
                        override4=struct.unpack('I', munch(4))[0]
                        dist=struct.unpack('I', munch(4))[0]
                        assert(len(munch(4).rstrip(b'\0'))==0)
                        comment=munch(300).rstrip(b'\0')
                        output(o,'Normal: Style %s, Distance %s, Overrides %s%s%s%s'%(style,dist,override1,override2,override3,override4))
                        if(len(comment)>0):
                            output(o,'     Comment: %s'%comment.decode('latin1'))
                    elif(p_type==2):
                        munch(4*17)
                        assert(len(munch(4).rstrip(b'\0'))==0)
                        comment=munch(300).rstrip(b'\0')
                        output(o,'Inactive:')
                        if(len(comment)>0):
                            output(o,'     Comment: %s'%comment.decode('latin1'))
                    elif(p_type==3):
                        munch(4)
                        id=struct.unpack('I', munch(4))[0]
                        munch(4*9)
                        override1=struct.unpack('I', munch(4))[0]
                        style=struct.unpack('I', munch(4))[0]
                        override2=struct.unpack('I', munch(4))[0]
                        override3=struct.unpack('I', munch(4))[0]
                        override4=struct.unpack('I', munch(4))[0]
                        dist=struct.unpack('I', munch(4))[0]
                        assert(len(munch(4).rstrip(b'\0'))==0)
                        comment=munch(300).rstrip(b'\0')
                        output(o,'Platform: Id %s, Style %s, Distance %s, Overrides %s%s%s%s'%(id,style,dist,override1,override2,override3,override4))
                        if(len(comment)>0):
                            output(o,'     Comment: %s'%comment.decode('latin1'))
                    elif(p_type==4):
                        path_output='Path: '
                        for i in range(0,10):
                            path_output=''.join([path_output,str(struct.unpack('I', munch(4))[0]),', '])
                        path_output=''.join([path_output,str(struct.unpack('I', munch(4))[0])])
                        munch(4*6)
                        assert(len(munch(4).rstrip(b'\0'))==0)
                        comment=munch(300).rstrip(b'\0')
                        output(o,path_output)
                        if(len(comment)>0):
                            output(o,'     Comment: %s'%comment.decode('latin1'))
                    if(p_type==5):
                        munch(4*11)
                        override1=struct.unpack('I', munch(4))[0]
                        style=struct.unpack('I', munch(4))[0]
                        override2=struct.unpack('I', munch(4))[0]
                        override3=struct.unpack('I', munch(4))[0]
                        override4=struct.unpack('I', munch(4))[0]
                        dist=struct.unpack('I', munch(4))[0]
                        assert(len(munch(4).rstrip(b'\0'))==0)
                        comment=munch(300).rstrip(b'\0')
                        output(o,'Border:')
                        if(len(comment)>0):
                            output(o,'     Comment: %s'%comment.decode('latin1'))
                    number_of_vertices = struct.unpack('I', munch(4))[0]
                    points = []
                    for __ in range(number_of_vertices):
                        x = struct.unpack('d', munch(8))[0]
                        y = struct.unpack('d', munch(8))[0]
                        output(o,'     % 8.3f   % 8.3f'%(x,y*(-1)))
                if(topo==0):
                    output(o,'')
                    number_of_poly_vertices=struct.unpack('I', munch(4))[0]
                    output(o,'Lines: %s'%number_of_poly_vertices)
                    for _ in range(number_of_poly_vertices):
                        x = struct.unpack('d', munch(8))[0]
                        y = struct.unpack('d', munch(8))[0]
                        dx=struct.unpack('d', munch(8))[0]
                        dy=struct.unpack('d', munch(8))[0]
                        x2 = x+dx
                        y2 = y+dy
                        dx_unit=struct.unpack('d', munch(8))[0]
                        dy_unit=struct.unpack('d', munch(8))[0]
                        length=struct.unpack('d', munch(8))[0]
                        assert(abs(dx_unit*dx_unit+dy_unit*dy_unit-1)<0.00001)
                        assert(abs(length*length-dx*dx-dy*dy)<0.00001)
                        assert(abs(dx_unit-dx/length)<0.00001)
                        assert(abs(dy_unit-dy/length)<0.00001)
                        textureid=struct.unpack('I', munch(4))[0]
                        solid=struct.unpack('I', munch(4))[0]
                        dist=struct.unpack('I', munch(4))[0]
                        visible=struct.unpack('I', munch(4))[0]
                        if(printPolyLineObjects):
                            output(o,'Line: %s, %s, Dist: %s, Texture: %s'%('Solid' if solid==0 else 'Unsolid','Invisible' if visible==1 else 'Visible',dist,textureid))
                            output(o,'     % 8.3f   % 8.3f'%(x,y))
                            output(o,'     % 8.3f   % 8.3f'%(x2,y2))
                    output(o,'')
                    #output(o,'UnknownFloat: %8.15f'%struct.unpack('d', munch(8))[0])
                    assert(abs(struct.unpack('d', munch(8))[0]-60.0)<0.00001)
                    output(o,'Rand: %s'%struct.unpack('i', munch(4))[0])
                    
                    while 1==1:
                        hasplatforms=struct.unpack('B', munch(1))[0]
                        if(hasplatforms==0):
                            break
                        output(o,'')
                        output(o,'Platform:')
                        xmin05=struct.unpack('d', munch(8))[0]
                        ymin05=struct.unpack('d', munch(8))[0]
                        xmin=struct.unpack('d', munch(8))[0]
                        ymin=struct.unpack('d', munch(8))[0]
                        output(o,'     Min xy:    % 8.3f   % 8.3f'%(xmin,ymin))
                        xmax=struct.unpack('d', munch(8))[0]
                        ymax=struct.unpack('d', munch(8))[0]
                        output(o,'     Max xy:    % 8.3f   % 8.3f'%(xmax,ymax))
                        xmin33=struct.unpack('d', munch(8))[0]
                        ymin33=struct.unpack('d', munch(8))[0]
                        xmin2=struct.unpack('d', munch(8))[0]
                        ymin2=struct.unpack('d', munch(8))[0]
                        xmax2=struct.unpack('d', munch(8))[0]
                        ymax2=struct.unpack('d', munch(8))[0]
                        objectx=struct.unpack('d', munch(8))[0]
                        objecty=struct.unpack('d', munch(8))[0]
                        output(o,'     Object xy: % 8.3f   % 8.3f'%(objectx,objecty))
                        assert(abs(xmin-xmin2)<0.00001)
                        assert(abs(ymin-ymin2)<0.00001)
                        assert(abs(xmax-xmax2)<0.00001)
                        assert(abs(ymax-ymax2)<0.00001)
                        assert(abs(xmin-xmin05-0.05)<0.00001)
                        assert(abs(xmin-xmin33-0.33)<0.00001)
                        assert(abs(ymin-ymin05-0.05)<0.00001)
                        assert(abs(ymin-ymin33-0.33)<0.00001)
                        
                        imagewidth=struct.unpack('I', munch(4))[0]
                        imageheight=struct.unpack('I', munch(4))[0]
                        output(o,'     Size:  %5i %5i'%(imagewidth,imageheight))
                        int2=struct.unpack('I', munch(4))[0]
                        int3=struct.unpack('I', munch(4))[0]
                        output(o,'     Size2: %5i %5i'%(int2,int3))
                        platformid=struct.unpack('I', munch(4))[0]
                        output(o,'     Id: %i'%(platformid))
                        int5=struct.unpack('I', munch(4))[0]
                        assert(int5==0)
                        output(o,'     Int5: %i'%(int5))
                        int6=struct.unpack('I', munch(4))[0]
                        assert(int6==0)
                        output(o,'     Int6: %i'%(int6))
                        int7=struct.unpack('I', munch(4))[0]
                        assert(int7==0)
                        output(o,'     Int7: %i'%(int7))
                        int8=struct.unpack('I', munch(4))[0]
                        assert(int8==0)
                        output(o,'     Int8: %i'%(int8))
                        int9=struct.unpack('I', munch(4))[0]
                        assert(int9==0)
                        output(o,'     Int9: %i'%(int9))
                        int10=struct.unpack('I', munch(4))[0]
                        assert(int10==0)
                        output(o,'     Int10: %i'%(int10))
                        int11=struct.unpack('I', munch(4))[0]
                        assert(int11==0)
                        output(o,'     Int11: %i'%(int11))
                        tex_id=struct.unpack('I', munch(4))[0]
                        border_id=struct.unpack('I', munch(4))[0]
                        output(o,'     Texture: %3i Border: %3i'%(tex_id,border_id))
                        assert(struct.unpack('I', munch(4))[0]==1478256389)
                        int15padding=struct.unpack('I', munch(4))[0]
                        #output(o,'     Int15: %i'%(int15padding))
                        assert(int15padding==0)
                        total_polygon_vertices_in_platform=struct.unpack('I', munch(4))[0]
                        output(o,'Lines: %s'%total_polygon_vertices_in_platform)
                        for _ in range(total_polygon_vertices_in_platform):
                            x = struct.unpack('d', munch(8))[0]
                            y = struct.unpack('d', munch(8))[0]
                            dx=struct.unpack('d', munch(8))[0]
                            dy=struct.unpack('d', munch(8))[0]
                            x2 = x+dx
                            y2 = y+dy
                            dx_unit=struct.unpack('d', munch(8))[0]
                            dy_unit=struct.unpack('d', munch(8))[0]
                            length=struct.unpack('d', munch(8))[0]
                            assert(abs(dx_unit*dx_unit+dy_unit*dy_unit-1)<0.00001)
                            assert(abs(length*length-dx*dx-dy*dy)<0.00001)
                            assert(abs(dx_unit-dx/length)<0.00001)
                            assert(abs(dy_unit-dy/length)<0.00001)
                            textureid=struct.unpack('I', munch(4))[0]
                            solid=struct.unpack('I', munch(4))[0]
                            dist=struct.unpack('I', munch(4))[0]
                            visible=struct.unpack('I', munch(4))[0]
                            assert(textureid==0)
                            assert(solid==0)
                            assert(dist==0)
                            assert(visible==0)
                            if(printPlatformLineObjects):
                                output(o,'Line:')
                                output(o,'     % 8.3f   % 8.3f'%(x,y))
                                output(o,'     % 8.3f   % 8.3f'%(x2,y2))
                    assert munch(1)==b'\x58'
                    output(o,'')
                    double1=struct.unpack('d', munch(8))[0]
                    output(o,'     float1: %i'%double1)
                    double2=struct.unpack('d', munch(8))[0]
                    output(o,'     float2: %i'%double2)
                    #assert(abs(struct.unpack('d', munch(8))[0]-(-406.64166666666665))<0.00001)
                    #assert(abs(struct.unpack('d', munch(8))[0]-(-74.99166666666666))<0.00001)
                    
                    while 1==1:
                        haspath=struct.unpack('B', munch(1))[0]
                        if(haspath==0):
                            break
                        output(o,'')
                        output(o,'Path:')
                        assert(len(munch(24).rstrip(b'\0'))==0)
                        pathid=struct.unpack('I',munch(4))[0]
                        output(o,'     pathid: %i'%pathid)
                        platformid=struct.unpack('I',munch(4))[0]
                        output(o,'     platformid: %i'%platformid)
                        pathsubtype=struct.unpack('I',munch(4))[0]
                        output(o,'     pathsubtype: %i'%pathsubtype)
                        speedattop=struct.unpack('I',munch(4))[0]
                        output(o,'     speedattop: %i'%speedattop)
                        speedatbottom=struct.unpack('I',munch(4))[0]
                        output(o,'     speedatbottom: %i'%speedatbottom)
                        opendelay=struct.unpack('I',munch(4))[0]
                        output(o,'     opendelay: %i'%opendelay)
                        closedelay=struct.unpack('I',munch(4))[0]
                        output(o,'     closedelay: %i'%closedelay)
                        reopendelay=struct.unpack('I',munch(4))[0]
                        output(o,'     reopendelay: %i'%reopendelay)
                        reclosedelay=struct.unpack('I',munch(4))[0]
                        output(o,'     reclosedelay: %i'%reclosedelay)
                        initially=struct.unpack('I',munch(4))[0]
                        output(o,'     initially: %i'%initially)
                        noplatforms=struct.unpack('I',munch(4))[0]
                        output(o,'     noplatforms: %i'%noplatforms)
                        assert(len(munch(16).rstrip(b'\0'))==0)
                        framesforward=struct.unpack('I',munch(4))[0]
                        output(o,'     framesforward: %i'%framesforward)
                        framestotal=struct.unpack('I',munch(4))[0]
                        output(o,'     framestotal: %i'%framestotal)
                        assert(len(munch(4).rstrip(b'\0'))==0)
                        int0=struct.unpack('I',munch(4))[0]
                        output(o,'     Int0: %i'%int0)
                        int1=struct.unpack('I',munch(4))[0]
                        output(o,'     Int1: %i'%int1)
                        int2=struct.unpack('I',munch(4))[0]
                        output(o,'     Int2: %i'%int2)
                        int3=struct.unpack('I',munch(4))[0]
                        output(o,'     Int3: %i'%int3)
                        int4=struct.unpack('I',munch(4))[0]
                        output(o,'     Int4: %i'%int4)
                        assert(len(munch(20).rstrip(b'\0'))==0)
                        assert(len(munch(6*4).rstrip(b'\0'))==0)
                        assert(struct.unpack('I', munch(4))[0]==1236278137)
                        assert(len(munch(4).rstrip(b'\0'))==0)
                        output(o,'')
                        
                        path_output='     mozg_x: '
                        for i in range(framestotal):
                            path_output=''.join([path_output,str(struct.unpack('i', munch(4))[0]),', '])
                        output(o,path_output)
                        assert(munch(1)==b'\x15')
                        path_output='     mozg_y: '
                        for i in range(framestotal):
                            path_output=''.join([path_output,str(struct.unpack('i', munch(4))[0]),', '])
                        output(o,path_output)
                        assert(munch(1)==b'\x15')
                        path_output='     seb_x: '
                        for i in range(framestotal):
                            path_output=''.join([path_output,str(struct.unpack('i', munch(4))[0]),', '])
                        output(o,path_output)
                        assert(munch(1)==b'\x15')
                        path_output='     seb_y: '
                        for i in range(framestotal):
                            path_output=''.join([path_output,str(struct.unpack('i', munch(4))[0]),', '])
                        output(o,path_output)
                        assert(munch(1)==b'\x15')
                        path_output='     plat_x: '
                        for i in range(framestotal):
                            path_output=''.join([path_output,str(struct.unpack('i', munch(4))[0]),', '])
                        output(o,path_output)
                        assert(munch(1)==b'\x15')
                        path_output='     plat_y: '
                        for i in range(framestotal):
                            path_output=''.join([path_output,str(struct.unpack('i', munch(4))[0]),', '])
                        output(o,path_output)
                        assert(munch(1)==b'\x15')
                            
                assert munch(1) == b'\x2B'
                assert struct.unpack('I', munch(4))[0]==192356446
                
                output(o,'')
                number_of_objects = int(struct.unpack('d', munch(8))[0])
                output(o,'Object#: %s'%number_of_objects)
                
                for _ in range(number_of_objects):
                    x = struct.unpack('d', munch(8))[0]
                    y = struct.unpack('d', munch(8))[0]
                    path_output='Object: '
                    path_output=''.join([num2obj[struct.unpack('I', munch(4))[0]-1],': '])
                    for i in range(0,5):
                        path_output=''.join([path_output,str(struct.unpack('I', munch(4))[0]),', '])
                    path_output=''.join([path_output,str(struct.unpack('I', munch(4))[0])])
                    output(o,path_output)
                    output(o,'     % 8.3f   % 8.3f'%(x,y))
                    comment=munch(300).rstrip(b'\0')
                    if(len(comment)>0):
                        output(o,'     Comment: %s'%comment.decode('latin1'))
              
                output(o,'')
                number_of_pictures = int(struct.unpack('d', munch(8))[0])
                output(o,'Picture#: %s'%number_of_pictures)
                for _ in range(number_of_pictures):
                    name=munch(10)
                    name=name[:name.find(b'\0')].decode('latin1')
                    text=munch(10)
                    text=text[:text.find(b'\0')].decode('latin1')
                    mask=munch(10)
                    mask=mask[:mask.find(b'\0')].decode('latin1')
                    assert(len(text)==0)
                    assert(len(mask)==0)
                    x = struct.unpack('d', munch(8))[0]
                    y = struct.unpack('d', munch(8))[0]
                    distance = struct.unpack('I', munch(4))[0]
                    clipping = struct.unpack('I', munch(4))[0]
                    output(o,'%s: %s %s'%(name,distance,clipping2txt[clipping]))
                    
                        
                
    o.close()

allLevels=False;
silence=False;
printPolyLineObjects=True;
printPlatformLineObjects=True;
LoopLevel()