from PIL import Image
import tkinter as tk
import tkinter.filedialog as filedialog
import os
import sys
import struct

#.tga source info for 16-bit: http://www.ryanjuckett.com/programming/parsing-colors-in-a-tga-file/
    
def unrle(data):
    unrle_data=b''
        
    i=0
    datalen=len(data)
    while i<datalen:
        if(data[i]&0b10000000==0b10000000):
            unrle_data=b''.join([unrle_data,data[i+1:i+3]*(data[i]-0b10000000+1)])
            i+=3
        else:
            unrle_data=b''.join([unrle_data,data[i+1:i+1+(data[i]+1)*2]])
            i+=1+(data[i]+1)*2
    return unrle_data
            
    
    
def uncorruptTGA(file,file2):
    #print(file)
    with open(file,'rb') as f:
        data = f.read()
        assert(data[0:12]==b'\x00\x00\x0A\x00\x00\x00\x00\x00\x00\x00\x00\x00')
        width=struct.unpack('H', data[12:14])[0]
        height=struct.unpack('H', data[14:16])[0]
        #print("Size - %s x %s: %s"%(width,height,width*height))
        assert(data[16:18]==b'\x10\x20')
        data=unrle(data[18:])
        #print("Total data - %s"%len(data))
        
        assert len(data)==width*height*2
        datalen=len(data)
        
        img=Image.new("RGB",(width,height))
        i=0
        j=0
        x=0
        while x<datalen:
            img.putpixel((i,j),
                (8*((data[x+1]&0b11111000)>>3),
                4*(((data[x+1]&0b00000111)<<3)+((data[x]&0b11100000)>>5)),
                8*(data[x]&0b00011111)))
            x+=2
            i+=1
            if(i==width):
                i=0
                j+=1
        img.save('%s%s'%(file2[:-4],'.tga'))
        img.save('%s%s'%(file2[:-4],'.png'))
    return


class Application(tk.Frame):
    def __init__(self,master):
        super().__init__(master)
        self.pack()
        self.create_widgets()
        
    def create_widgets(self):
        self.frame1=tk.Frame(self)
        self.frame1.pack(side="top")
        self.button1=tk.Button(self.frame1,text="Choose image file",command=self.SelectPopup1)
        self.button1.pack(side="left")
        self.button2=tk.Button(self.frame1,text="Choose folder",command=self.SelectPopup2)
        self.button2.pack(side="left")
        self.entry4=tk.Entry(self,width=80)
        self.entry4.insert(0,"file or directory here")
        self.entry4.pack(side="top")
        self.label8=tk.Label(self,text=" ")
        self.label8.pack(side="top")
        
        self.button6=tk.Button(self,text="***Uncorrupt TGAs (folder/.tga)***",command=self.OpenStuff,fg="red")
        self.button6.pack(side="top")
        
    def SelectPopup1(self):
        imgfile=filedialog.askopenfilename(filetypes=[("Image files",("*.tga","*.png","*.bmp","*.pcx")),("All","*")])
        self.entry4.delete(0,len(self.entry4.get()))
        self.entry4.insert(0,imgfile)
    def SelectPopup2(self):
        imgfile=filedialog.askdirectory()
        self.entry4.delete(0,len(self.entry4.get()))
        self.entry4.insert(0,imgfile)

    def OpenStuff(self):
        file=self.entry4.get()
        if(file[-4:].lower()==".tga"):
            print("Fixed files will be stored in program folder.\nProcessing: %s"%(file))
            uncorruptTGA(file,os.path.basename(file))
        else:
            print("Fixed files will be stored in program folder.")
            for file2 in os.listdir(file):
                if(file2.lower().endswith(".tga")):
                    #print("Processing: %s"%(file2))
                    try:
                        uncorruptTGA(file+"/"+file2,file2)
                    except:
                        print("Skipping: %s"%file2)
                        print("Skipping as header does not indicate corrupted! %s",sys.exc_info()[0])
        print("Done!\n\n\n")
        return
        
root=tk.Tk()
root.title("sunl's TGA uncorrupter")
app=Application(master=root)

app.mainloop()