import tkinter as tk
import tkinter.filedialog as filedialog
import os
import sys
import struct
import shutil
import time

class Application(tk.Frame):
    def __init__(self,master):
        super().__init__(master)
        self.pack()
        self.create_widgets()
        
    def create_widgets(self):
        self.frame1=tk.Frame(self)
        self.frame1.pack(side="top")
        self.button1=tk.Button(self.frame1,text="Choose .exe/.dat file",command=self.SelectPopup1)
        self.button1.pack(side="top")
        self.entry4=tk.Entry(self,width=80)
        self.entry4.insert(0,"exe path")
        self.entry4.pack(side="top")
        self.button2=tk.Button(self.frame1,text="Choose .txt",command=self.SelectPopup2)
        self.button2.pack(side="top")
        self.entry3=tk.Entry(self,width=80)
        self.entry3.insert(0,"txt path")
        self.entry3.pack(side="top")
        self.label8=tk.Label(self,text=" ")
        self.label8.pack(side="top")
        self.button6=tk.Button(self,text="Patch elma2.exe***",command=self.OpenStuff,fg="red")
        self.button6.pack(side="top")
        self.button7=tk.Button(self,text="Alovolt button finder state.dat***",command=self.OpenDat)
        self.button7.pack(side="top")
        
    def SelectPopup1(self):
        imgfile=filedialog.askopenfilename(filetypes=[("Exe/Dat file",("*.exe","*.dat")),("All","*")])
        self.entry4.delete(0,len(self.entry4.get()))
        self.entry4.insert(0,imgfile)
    def SelectPopup2(self):
        imgfile=filedialog.askopenfilename(filetypes=[("Text file","*.txt"),("All","*")])
        self.entry3.delete(0,len(self.entry4.get()))
        self.entry3.insert(0,imgfile)

    def OpenStuff(self):
        exe=self.entry4.get()
        txt=self.entry3.get()
        if(exe[-4:].lower()!=".exe"):
            print("Not .exe! Aborting")
            return
        if not(os.path.exists("backup")):
            print("ok")
            os.mkdir("backup")
            print("Made backup/ directory")
        backuppath="backup/%s_%s.exe.bak"%(os.path.basename(exe)[:-4],time.strftime("%Y%m%d%H%M%S",time.gmtime()))
        shutil.copy2(exe,backuppath)
        print("Saved backup file: %s"%backuppath)
        
        with open(txt,"r") as f:
            contents=0
            with open(exe,"rb") as g:
                contents=bytearray(g.read())
                #print(len(contents))
                commands=f.read().splitlines()
                for command in commands:
                    if(len(command)==0):
                        continue
                    command=command.split(",")
                    if(len(command[0])==0):
                        continue
                    i=int(command[0],16)
                    if(command[1].lower()=='double'):
                        contents[i:i+8]=struct.pack('<d',float(command[2]))
                    else:
                        instructions=command[1].split(" ")
                        for instruction in instructions:
                            #print(i)
                            contents[i]=int(instruction,16)
                            #g.write(struct.pack('B',int(instruction,16)))
                            i+=1
            with open(exe,"wb") as g:
                g.write(contents)
        print("Done! File successfully patched!\n\n\n")
        return
        
    def OpenDat(self):
        dat=self.entry4.get()
        print(dat[-4:].lower())
        if(dat[-4:].lower()!=".dat"):
            print("Not .dat! Aborting")
            return
        print("Note that you need to Esc out of the Elma2 Options menu back to the Main Menu for the changes to be saved to state.dat")
        with open(dat,'rb') as f:
            data = iter(f.read())
            def munch(n):
                return b''.join([bytes(chr(next(data)), 'latin1') for _ in range(n)])
            assert munch(4)==b'\xD1\0\0\0'
            munch(4)
            for i in range(20):
                playername=munch(16).rstrip(b'\0').decode('latin1')
                if(len(playername)>0):
                    print("Key codes for %s:"%playername)
                    munch(104+16)
                    thrust=struct.unpack('I', munch(4))[0]
                    brake=struct.unpack('I', munch(4))[0]
                    lrot=struct.unpack('I', munch(4))[0]
                    rrot=struct.unpack('I', munch(4))[0]
                    turn=struct.unpack('I', munch(4))[0]
                    print('     Thrust: %X'%thrust)
                    print('     Brake: %X'%brake)
                    print('     Left rotation: %X'%lrot)
                    print('     Right rotation: %X'%rrot)
                    print('     Turn: %X'%turn)
        return
      
      
     
root=tk.Tk()
root.title("sunl's .exe patcher v1.01")
app=Application(master=root)

app.mainloop()